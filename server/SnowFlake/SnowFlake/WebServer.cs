﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SnowFlake
{
    /// <summary>
    /// WebServer that accepts and process requests from app clients
    /// </summary>
    public class WebServer
    {
        public readonly int Port;
        private readonly HttpListener listener;

        public WebServer(int port)
        {
            this.Port = port;
            this.listener = new HttpListener();
            this.listener.Prefixes.Add(string.Format("http://*:{0}/", port));
            this.listener.Start();
        }

        public async void Start()
        {
            while (true)
            {
                Console.WriteLine("Waiting for a connection...");
                HttpListenerContext context = await this.listener.GetContextAsync();
                this.ProcessIncomingRequestAsync(context);
            }
        }

        private void Close()
        {
            this.listener.Close();
        }

        private async void ProcessIncomingRequestAsync(HttpListenerContext context)
        {
            if (context == null)
            {
                Console.WriteLine(string.Format("HttpListener failed to accept a connection"));
                return;
            }

            Console.WriteLine("Connected!");
            JToken returnedJson = null;

            try
            {
                returnedJson = await WebServer.StartProcessingRequest(context.Request);
                SendToClient(context.Response, returnedJson.ToString(), (int)HttpStatusCode.OK);
                Console.WriteLine("Respond request succeeded");
            }
            catch (Newtonsoft.Json.JsonReaderException jre)
            {
                AskClientRetryOnBadRequest(context.Response);
                Console.WriteLine(string.Format("Failed to process response. Exception: {0}", jre.Message));
                return;
            }
            catch (InvalidCastException ice)
            {
                AskClientRetryOnBadRequest(context.Response);
                Console.WriteLine(string.Format("Failed to process response. Exception: {0}", ice.Message));
                return;
            }
            catch (Exception e)
            {
                AskClientRetryOnBadRequest(context.Response);
                Console.WriteLine(string.Format("Failed to process response because {0}", e.Message));
                return;
            }
        }

        /// <summary>
        /// Processes request from client.
        /// </summary>
        /// <param name="client">returned tcpclient after accepting a connection</param>
        public static async Task<JToken> StartProcessingRequest(HttpListenerRequest request)
        {
            try
            {
                using (StreamReader reader = new StreamReader(request.InputStream))
                {
                    string body = reader.ReadToEnd();
                    if (string.IsNullOrEmpty(body) || body.Length < 10)
                    {
                        throw new Exception("Incoming request is empty or too short!");
                    }
                    Console.WriteLine(body);
                    JObject json = JObject.Parse(body);
                    Task<JToken> task = ClientRequest.parseClientRequestByRequestType(json);

                    return await task;
                }
            }
            catch(Exception e)
            {
                Console.WriteLine("Fail to process request because {0}", e.Message);
                return null;
            }
        }

        /// <summary>
        /// Returning 400 bad request if client's request contains invalid or empty data
        /// </summary>
        private static void AskClientRetryOnBadRequest(HttpListenerResponse response)
        {
            WebServer.SendToClient(response, "Bad Request", (int)HttpStatusCode.BadRequest);
        }

        /// <summary>
        /// Helper method to send back to client
        /// </summary>
        /// <param name="response"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        private static bool SendToClient(HttpListenerResponse response, string responseString, int httpStatusCode)
        {
            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(responseString);

            response.StatusCode = httpStatusCode;  
            response.StatusDescription = "SUCCESS";
            System.IO.Stream output = response.OutputStream;

            output.Write(buffer, 0, buffer.Length);
            output.Close();
            return true;
        }
    }
}
