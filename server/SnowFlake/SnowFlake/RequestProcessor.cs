﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.IO;
using StackExchange.Redis;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SnowFlake.DatabaseTypeClass;

namespace SnowFlake
{
    class RequestProcessor
    {
        /// <summary>
        /// Constructor of RequestProcessor
        /// </summary>
        public RequestProcessor()
        {
        }

        /// <summary>
        /// Verify Token from user
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        private static async Task<JObject> VerifyToken(JToken encodedToken)
        {
            string encodedTokenString = encodedToken.ToString();
            byte[] tokenByteArray = System.Convert.FromBase64String(encodedTokenString);
            Console.WriteLine(System.Text.Encoding.Default.GetString(tokenByteArray));
            JObject token = JObject.Parse(System.Text.Encoding.Default.GetString(tokenByteArray));

            if (token == null)
            {
                //null token
                throw new Exception("NULL input at VerifiyToken");
            }

            JToken userGuidToken = token["UserGuid"];
            JToken userPwdToken = token["UserPassword"];
            JToken userDeviceToken = token["DeviceToken"];
            JObject userJobj = null;

            if (userGuidToken == null || userPwdToken == null || userDeviceToken == null)
            {
                //Invalid Token
                throw new Exception("Invalid Token at VerifyToken");
            }

            IDatabase db = Database.GetRedisInstance().GetDatabase();

            string userJson = await db.StringGetAsync(userGuidToken.ToString());

            if (string.IsNullOrEmpty(userJson))
            {
                //Not found in DB
                throw new Exception("User Not Found at VerifyToken");
            }
            userJobj = JObject.Parse(userJson);

            JToken userpwd = userJobj["UserPassword"];
            JToken userdevice = userJobj["DeviceToken"];
            if (userpwd == null || userdevice == null ||
                userPwdToken == null || userDeviceToken == null ||
                !JToken.DeepEquals(userpwd, userPwdToken) || !JToken.DeepEquals(userdevice, userDeviceToken))
            {
                //token incorrect
                throw new Exception("invalid Token at VerifyToken");
            }

            Console.WriteLine("Verify Token Success!");
            return userJobj;
        }

        public static string EncodeToken(JObject token)
        {
            string encoded = System.Convert.ToBase64String(Encoding.ASCII.GetBytes(token.ToString()));
            return encoded;
        }

        /// <summary>
        /// Verify Token from user
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        private static async Task<JObject> VerifyTokenFromJobj(JObject token)
        {
            if (token == null)
            {
                //null token
                throw new Exception("NULL input at VerifiyToken");
            }

            JToken userGuidToken = token["UserGuid"];
            JToken userPwdToken = token["UserPassword"];
            JObject userJobj = null;

            if (userGuidToken == null || userPwdToken == null)
            {
                //Invalid Token
                throw new Exception("Invalid Token at VerifyToken");
            }

            IDatabase db = Database.GetRedisInstance().GetDatabase();

            string userJson = await db.StringGetAsync(userGuidToken.ToString());

            if (string.IsNullOrEmpty(userJson))
            {
                //Not found in DB
                throw new Exception("User Not Found at VerifyToken");
            }
            userJobj = JObject.Parse(userJson);

            JToken userpwd = userJobj["UserPassword"];
            JToken userdevice = userJobj["DeviceToken"];
            if (userpwd == null || userPwdToken == null || !JToken.DeepEquals(userpwd, userPwdToken))
            {
                //token incorrect
                throw new Exception("invalid Token at VerifyToken");
            }

            Console.WriteLine("Verify Token Success!");
            return userJobj;
        }

        public static async Task<JToken> NewFlake (JObject input)
        {
            JToken token = input["Token"];
            JObject flake = (JObject)input["Flake"];
            JObject question = (JObject)input["Question"];

            //Verify token
            JObject userJobj = await RequestProcessor.VerifyToken(token);
               
            flake.AddFirst(
                    new JProperty(FlakeConstant.Flake.FlakeGuid.ToString(),
                                  FlakeConstant.FlakePrefix + Guid.NewGuid().ToString()));

            //Make sure userGuid is consistent with token
            flake[FlakeConstant.Flake.UserGuid.ToString()] = userJobj["UserGuid"];
            flake[FlakeConstant.Flake.OpenCount.ToString()] = "10"; //enforce counter
            flake[FlakeConstant.Flake.CreateTimeStamp.ToString()] = DateTime.UtcNow.ToString();
            
            if (question != null)
            {
                question.AddFirst(
                    new JProperty(FlakeConstant.Question.QuestionGuid.ToString(),
                                  FlakeConstant.QuestionPrefix + Guid.NewGuid().ToString()));

                flake.Add(new JProperty(FlakeConstant.Question.QuestionGuid.ToString(), question[FlakeConstant.Question.QuestionGuid.ToString()]));
            }

            //database instance for accessing redis
            IDatabase db = Database.GetRedisInstance().GetDatabase();

            //adding to database
            //adding new entry to flake table
            await db.StringSetAsync(flake[FlakeConstant.Flake.FlakeGuid.ToString()].ToString(), flake.ToString());

            //construct miniFlake JObject
            MiniFlake miniFlakeObject = flake.ToObject<MiniFlake>();
            string miniFlakeJson = JsonConvert.SerializeObject(miniFlakeObject, Formatting.Indented);
            string miniFlakeGuid = userJobj[FlakeConstant.Flake.FlakeListGuid.ToString()].ToString();

            //adding new entry to Flake list of user
            await db.ListLeftPushAsync(miniFlakeGuid, miniFlakeJson);

            //push MiniFlake into Bucket
            string bucketKey = FlakeConstant.FlakeBucketPrefix + string.Format("{0},{1}", miniFlakeObject.Latitude.Split('.')[0], miniFlakeObject.Longitude.Split('.')[0]);
            await db.ListRightPushAsync(bucketKey, miniFlakeJson);
            
            if (question != null)
            {
                //adding new entry to question table if it exist
                await db.StringSetAsync(question[FlakeConstant.Question.QuestionGuid.ToString()].ToString(), question.ToString());
            }

            //TODO TODO TODO
            //Add to BucketList for search

            Console.WriteLine("New Flake Success!");
            return flake;
        }

        public static async Task<JToken> GetFlakesAsOwner(JObject input)
        {
            if (input == null)
            {
                //null input
                throw new Exception("Null Input at GetFlakesAsOwner");
            }

            JToken token = input["Token"];
            JObject userJobj = await RequestProcessor.VerifyToken(token);
            
            IDatabase db = Database.GetRedisInstance().GetDatabase();

            JObject result = new JObject();
            JArray flakeListJobject = new JArray();
            result.Add("FlakeList", flakeListJobject);

            if (userJobj["FlakeListGuid"] == null)
            {
                return result;
            }

            RedisValue[] flakeList = await db.ListRangeAsync(userJobj[FlakeConstant.User.FlakeListGuid.ToString()].ToString());
            string[] flakeListArray = flakeList.Select(key => (string)key).ToArray();

            Console.WriteLine(flakeListArray[0]);
            
            foreach (var item in flakeListArray)
            {
                flakeListJobject.Add(JObject.Parse(item));
            }
            
            result["FlakeList"] = flakeListJobject;
            return result;
        }

        public static async Task<JToken> OpenFlake(JObject input)
        {
            if (input == null)
            {
                //null input
                throw new Exception("Null Input at OpenFlake");
            }

            //Verify token
            JObject userJobj = await RequestProcessor.VerifyToken( input["Token"] );
            JObject flakeJobjReceived = (JObject)input["Flake"];

            if (flakeJobjReceived == null)
            {
                throw new Exception("Flake Not Found at OpenFlake");
            }

            JObject result = new JObject();

            IDatabase db = Database.GetRedisInstance().GetDatabase();
            string flakeGuid = flakeJobjReceived[FlakeConstant.Flake.FlakeGuid.ToString()].ToString();
            string flakeDbJson = await db.StringGetAsync(flakeGuid);
            JObject flakeDbJobj = JObject.Parse(flakeDbJson);

            string triedUserFlakeKey = FlakeConstant.UserTriedFlakePrefix +
                userJobj[FlakeConstant.User.UserGuid.ToString()].ToString() +
                flakeDbJobj[FlakeConstant.Flake.FlakeGuid.ToString()].ToString();

            //check if user tried open this flake already
            string tried = await db.StringGetAsync(triedUserFlakeKey);
            if (tried != null)
            {
                result.AddFirst(new JProperty("OpenStatus", "False"));
                return result;
            }
            await db.StringSetAsync(triedUserFlakeKey, "False");

            //checck flake OpenCount, Question Answered Correctly if there is one
            //QuesitonAnswerCheck && OpenCountCount from previous pull
            if (flakeJobjReceived[FlakeConstant.Flake.QuestionCorrectAnswer.ToString()] == null ||
                flakeDbJobj[FlakeConstant.Flake.QuestionCorrectAnswer.ToString()] == null ||
                !JToken.DeepEquals(flakeJobjReceived[FlakeConstant.Flake.QuestionCorrectAnswer.ToString()], flakeDbJobj[FlakeConstant.Flake.QuestionCorrectAnswer.ToString()]) ||
                Int32.Parse(flakeDbJobj[FlakeConstant.Flake.OpenCount.ToString()].ToString()) < 1)
            {
                result.AddFirst(new JProperty("OpenStatus", "False"));
                return result;
            }

            //OpenCount check
            RedisValue tokenForLock = Environment.MachineName;
            TimeSpan lockDuration = TimeSpan.FromSeconds(30);
            string flakeGuidDb = flakeDbJobj[FlakeConstant.Flake.FlakeGuid.ToString()].ToString();
            string lockKey = "Lock_" + flakeGuid;
            int count = -1;
            if (await db.LockTakeAsync(lockKey, tokenForLock, lockDuration))
            {
                try
                {
                    string flakeLockJson = await db.StringGetAsync(flakeJobjReceived[FlakeConstant.Flake.FlakeGuid.ToString()].ToString());
                    JObject flakeLockJobj = JObject.Parse(flakeDbJson);

                    count = Int32.Parse(flakeLockJobj[FlakeConstant.Flake.OpenCount.ToString()].ToString());
                    flakeLockJobj[FlakeConstant.Flake.OpenCount.ToString()] = (--count).ToString();
                    db.StringSet(flakeGuid, flakeLockJobj.ToString());
                }
                finally
                {
                    db.LockRelease(lockKey, tokenForLock);
                }
            }

            if (count < 0)
            {
                //TODO TODO TODO
                //Update Bucket List Here when OpenCount is less than 0 for a Flake
                result.AddFirst(new JProperty("OpenStatus", "False"));
                return result;
            }

            FlakeChat flakeChat = flakeDbJobj.ToObject<FlakeChat>();
            flakeChat.JoinUserGuid = userJobj[FlakeConstant.User.UserGuid.ToString()].ToString();
            flakeChat.JoinUserName = userJobj[FlakeConstant.User.UserName.ToString()].ToString();
            flakeChat.FlakeChatGuid = FlakeConstant.FlakeChatPrefix + Guid.NewGuid().ToString();

            string flakeChatJson = JsonConvert.SerializeObject(flakeChat);

            MiniFlakeChat miniFlakeChat = JsonConvert.DeserializeObject<MiniFlakeChat>(flakeChatJson);
            string miniFlakeChatJson = JsonConvert.SerializeObject(miniFlakeChat);
            string flakeChatListGuid1 = miniFlakeChat.UserGuid.Replace(FlakeConstant.UserPrefix, FlakeConstant.FlakeChatListPrefix);
            string flakeChatListGuid2 = miniFlakeChat.JoinUserGuid.Replace(FlakeConstant.UserPrefix, FlakeConstant.FlakeChatListPrefix);
            
            await db.ListRightPushAsync(flakeChat.FlakeChatGuid, flakeChatJson);
            await db.ListRightPushAsync(flakeChatListGuid1, miniFlakeChatJson);
            await db.ListRightPushAsync(flakeChatListGuid2, miniFlakeChatJson);

            //pushing new message to both user's update queue
            //Creator update
            string utcTime = DateTime.UtcNow.ToString();
            string alertStringForCreator = miniFlakeChat.JoinUserName +
                                           " picked up your FLAKE: " +
                                           flakeChat.Content;

            JObject updateMessageCreator = new JObject();
            updateMessageCreator.Add(FlakeConstant.Message.SenderUserGuid.ToString(), flakeChat.JoinUserGuid);
            updateMessageCreator.Add(FlakeConstant.Message.SenderUserName.ToString(), flakeChat.JoinUserName);
            updateMessageCreator.Add(FlakeConstant.Message.SendUtcTime.ToString(), utcTime);
            updateMessageCreator.Add(FlakeConstant.Message.FlakeChatGuid.ToString(), flakeChat.FlakeChatGuid);
            updateMessageCreator.Add(FlakeConstant.Message.MessageBody.ToString(), alertStringForCreator);
            
            await db.ListRightPushAsync(FlakeConstant.UserNotificaitonQueuePrefix+flakeChat.UserGuid, updateMessageCreator.ToString());

            //Picker update
            string alertStringForPicker = "You just picked up a FLAKE from " + flakeChat.UserName + " : " + flakeChat.Content;

            JObject updateMessagePicker = new JObject();
            updateMessagePicker.Add(FlakeConstant.Message.SenderUserGuid.ToString(), flakeChat.UserGuid);
            updateMessagePicker.Add(FlakeConstant.Message.SenderUserName.ToString(), flakeChat.UserName);
            updateMessagePicker.Add(FlakeConstant.Message.SendUtcTime.ToString(), utcTime);
            updateMessagePicker.Add(FlakeConstant.Message.FlakeChatGuid.ToString(), flakeChat.FlakeChatGuid);
            updateMessagePicker.Add(FlakeConstant.Message.MessageBody.ToString(), alertStringForPicker);

            await db.ListRightPushAsync(FlakeConstant.UserNotificaitonQueuePrefix + flakeChat.JoinUserGuid, updateMessageCreator.ToString());

            //Set Open Status For User to true
            await db.StringSetAsync(triedUserFlakeKey, "True");

            //look up the creator's device token for notification
            Task<RedisValue> creatorLookup = db.StringGetAsync(miniFlakeChat.UserGuid); 

            //Push notification to APN Server
            JObject alert = new JObject();
            alert.Add("alert", alertStringForCreator);
            JObject payload = new JObject();
            payload.Add("Aps", alert);
            JObject notification = new JObject();
            notification.Add("Payload", payload);

            string creatorJson = await creatorLookup;
            JObject creatorJobj = JObject.Parse(creatorJson);

            notification.Add(FlakeConstant.User.DeviceToken.ToString(), creatorJobj[FlakeConstant.User.DeviceToken.ToString()]);
            AppleNotification.SendNotification(notification);

            return JObject.Parse(flakeChatJson);
        }

        public static async Task<JToken> NewAccount(JObject input)
        {
            if (input == null)
            {
                //null input
                throw new Exception("Null Input at NewAccount");
            }

            JToken userGuidToken = new JValue(FlakeConstant.UserPrefix + input["UserGuid"].ToString());
            JToken userName = input["UserName"];
            JToken userPwdToken = input["UserPassword"];
            JToken userDeviceToken = input["DeviceToken"];

            // Validate input
            if (userGuidToken == null || userPwdToken == null || userName == null || userDeviceToken == null)
            {
                //Invalid Token
                throw new Exception("Invalid JSON Request at NewAccount");
            }

            // Construct a user JObject
            JObject user = new JObject();
            user.Add("UserGuid", userGuidToken.ToString());
            user.Add("UserName", userName);
            user.Add("UserPassword", userPwdToken);
            user.Add("CreateTimeStamp", DateTime.UtcNow.ToString());
            user.Add("DeviceToken", userDeviceToken);

            string ContactListGuid = FlakeConstant.ContactListPrefix + userGuidToken.ToString();
            string FlakeListGuid = FlakeConstant.FlakeListPrefix + userGuidToken.ToString();
            string FlakeChatListGuid = FlakeConstant.FlakeChatListPrefix + userGuidToken.ToString();

            user.Add("ContactListGuid", ContactListGuid);
            user.Add("FlakeListGuid", FlakeListGuid);
            user.Add("FlakeChatListGuid", FlakeChatListGuid);
            
            // Add user to database
            IDatabase db = Database.GetRedisInstance().GetDatabase();
            var transaction = db.CreateTransaction();
            transaction.AddCondition(Condition.KeyNotExists(userGuidToken.ToString()));
            transaction.StringSetAsync(userGuidToken.ToString(), user.ToString());
            if (!transaction.Execute())
            {
                //user exist
                throw new Exception("User Exist already at NewAccount");
            }

            // Return token to Client
            JObject userJobj = new JObject();
            userJobj.Add("UserGuid", userGuidToken);
            userJobj.Add("UserPassword", userPwdToken);
            userJobj.Add("DeviceToken", userDeviceToken);
            JObject token = new JObject();
            token.Add("Token", RequestProcessor.EncodeToken(userJobj));

            Console.WriteLine("Update Success!");
            return token;
        }

        public static async Task<JToken> SignIn(JObject input)
        {
            if (input == null)
            {
                //null input
                throw new Exception("Null Input at SignIn");
            }

            JToken userGuidToken = new JValue(FlakeConstant.UserPrefix + (input["UserGuid"]==null ? "": input["UserGuid"].ToString()) );
            JToken userNameToken = input["UserName"];
            JToken userpwdToken = input["UserPassword"];
            JToken deviceToken = input["DeviceToken"];

            if (userpwdToken == null || deviceToken == null)
            {
                throw new Exception("Invalid input at Sign in");
            }

            string userGuid = userGuidToken.ToString();

            JObject token = new JObject();
            token.AddFirst(new JProperty("UserGuid", userGuid));
            token.Add(new JProperty("UserPassword", userpwdToken));
            token.Add(new JProperty("DeviceToken", deviceToken));

            JObject userJobj = await RequestProcessor.VerifyTokenFromJobj(token);
            userJobj[FlakeConstant.User.DeviceToken.ToString()] = deviceToken;

            IDatabase db = Database.GetRedisInstance().GetDatabase();
            await db.StringSetAsync(userGuid, userJobj.ToString());
            
            JObject tokenWrapper = new JObject();
            tokenWrapper.Add("Token", RequestProcessor.EncodeToken(token));
            tokenWrapper.Add("UserName", userJobj[FlakeConstant.User.UserName.ToString()]);
            return tokenWrapper;
        }

        public static async Task<JToken> GetFlakeBuckets(JObject input)
        {
            /*
            {"LocationList": [{"lat": "25", "lon": "21"}, {"lat": "35", "lon": "35"}, {"lat": "45", "lon": "45"}]}
            */
            JObject userJobj = await RequestProcessor.VerifyToken(input["Token"]);

            JArray locationList = (JArray)input["LocationList"];
            JArray miniflakeListArray = new JArray();

            foreach (JObject obj in locationList)
            {
                JToken lat = obj["lat"];
                JToken lon = obj["lon"];

                string locationKey = FlakeConstant.FlakeBucketPrefix + string.Format("{0},{1}", lat.ToString(), lon.ToString());

                //TODO convert lat,log to coordinates for the bucketmap

                IDatabase db = Database.GetRedisInstance().GetDatabase();
                RedisValue[] miniflakeList = await db.ListRangeAsync(locationKey);
                string[] miniflakeListArrayTemp = miniflakeList.Select(key => (string)key).ToArray();
                
                foreach (string miniflake in miniflakeListArrayTemp)
                {
                    JObject miniflakeJobj = JObject.Parse(miniflake);
                    string flakeJson = await db.StringGetAsync((miniflakeJobj[FlakeConstant.MiniFlake.FlakeGuid.ToString()]).ToString());
                    JObject flakeJobj = JObject.Parse(flakeJson);

                    //check if this flake has been tried before
                    string userTriedFlakeBefore = FlakeConstant.UserTriedFlakePrefix +
                        userJobj[FlakeConstant.User.UserGuid.ToString()].ToString() +
                        flakeJobj[FlakeConstant.Flake.FlakeGuid.ToString()].ToString();
                    Task<RedisValue> checkTriedBeforeTask = db.StringGetAsync(userTriedFlakeBefore);

                    if (Int32.Parse(flakeJobj[FlakeConstant.Flake.OpenCount.ToString()].ToString()) < 0)
                    {
                        await db.ListRemoveAsync(locationKey, miniflake);
                    }
                    else if (userJobj[FlakeConstant.User.UserGuid.ToString()] != null &&
                             flakeJobj[FlakeConstant.Flake.UserGuid.ToString()] != null &&
                             !JToken.DeepEquals(userJobj[FlakeConstant.User.UserGuid.ToString()], flakeJobj[FlakeConstant.Flake.UserGuid.ToString()]) &&
                             string.IsNullOrEmpty(await checkTriedBeforeTask))
                    {
                        miniflakeListArray.Add(miniflakeJobj);
                        Console.WriteLine(miniflakeJobj.ToString());
                    }
                }
            }

            JObject flakeListJobject = new JObject();
            flakeListJobject.Add("MiniFlakes", miniflakeListArray);
            return flakeListJobject;
        }
        
        public static async Task<JToken> GetQuestion(JObject input)
        {
            JToken questionGuid = input["QuestionGuid"];
            JToken token = input["Token"];
            
            if(questionGuid == null || token == null)
            {
                throw new Exception("GetQuestion input validation failed!");
            }
            JObject userJobj = await RequestProcessor.VerifyToken(token);

            IDatabase db = Database.GetRedisInstance().GetDatabase();

            string questionFromDataBase = await db.StringGetAsync(questionGuid.ToString());
            JObject questionObject = JObject.Parse(questionFromDataBase);

            return questionObject;
        }

        //TODO TODO TODO
        //Get Update Queue
        public static async Task<JToken> GetUpdateQueue(JObject input)
        {
            if (input == null)
            {
                throw new Exception("Null input at GetUpdateQueue");
            }

            JToken token = input["Token"];
            JObject userJobj = await RequestProcessor.VerifyToken(token);

            JArray result = new JArray();
            IDatabase db = Database.GetRedisInstance().GetDatabase();

            string listKey = FlakeConstant.UserNotificaitonQueuePrefix + userJobj[FlakeConstant.User.UserGuid.ToString()].ToString();
            RedisValue[] updateListJson = await db.ListRangeAsync(listKey, 0);

            if (updateListJson != null)
            {
                foreach (var message in updateListJson)
                {
                    string messageJson = message;
                    JObject messageJobj = JObject.Parse(messageJson);
                    result.Add(messageJobj);
                }

                await db.KeyDeleteAsync(listKey);
            }

            JObject resultWrapper = new JObject();
            resultWrapper.Add("NotificationUpdates", result);

            return resultWrapper;
        }

        //TODO TODO TODO
        //Get FlakeChatList as owner
        public static async Task<JToken> GetFlakeChatListAsOwner(JObject input)
        {
            if (input == null)
            {
                throw new Exception("Null input at GetFlakeChatListAsOwner");
            }

            JToken token = input["Token"];
            JObject userJobj = await RequestProcessor.VerifyToken(token);
            
            IDatabase db = Database.GetRedisInstance().GetDatabase();
            JArray flakeListJobject = new JArray();
            JToken flakeChatListGuidJtoken = userJobj[FlakeConstant.User.FlakeChatListGuid.ToString()];

            if (flakeChatListGuidJtoken != null)
            {
                RedisValue[] flakeList = await db.ListRangeAsync(flakeChatListGuidJtoken.ToString());
                string[] flakeListArray = flakeList.Select(key => (string)key).ToArray();

                Console.WriteLine(flakeListArray[0]);
                foreach (var item in flakeListArray)
                {
                    flakeListJobject.Add(JObject.Parse(item));
                }
            }
            
            JObject result = new JObject();
            result.Add("FlakeList", flakeListJobject);

            return result;
        }

        //TODO TODO TODO
        //Send Message on FlakeChat
        public static async Task<JToken> SendFlakeMessage(JObject input)
        {
            if (input == null)
            {
                throw new Exception("Null input at SendFlakeMessage");
            }

            JObject userJobj = await RequestProcessor.VerifyToken(input["Token"]);
            JToken flakeChatGuid = input[FlakeConstant.FlakeChat.FlakeChatGuid.ToString()];
            JToken message = input["MessageContent"];
            JToken receiverGuid = input["ReceiverGuid"];

            if (flakeChatGuid == null || message == null || receiverGuid == null)
            {
                throw new Exception("Invalid input at SendFlakeMessage");
            }

            IDatabase db = Database.GetRedisInstance().GetDatabase();
            string flakeChatJson = await db.ListGetByIndexAsync(flakeChatGuid.ToString(), 0);

            if (flakeChatJson == null)
            {
                throw new Exception("Invalid FlakeChat Guid at SendFlakeMessage");
            }

            JObject flakeChatJobj = JObject.Parse(flakeChatJson);

            string utcTime = DateTime.UtcNow.ToString();
            string senderName = userJobj[FlakeConstant.User.UserName.ToString()].ToString();
            string senderGuid = userJobj[FlakeConstant.User.UserGuid.ToString()].ToString();
            string userGuid1FromDB = flakeChatJobj[FlakeConstant.FlakeChat.UserGuid.ToString()].ToString();
            string userGuid2FromDB = flakeChatJobj[FlakeConstant.FlakeChat.JoinUserGuid.ToString()].ToString();

            if ((string.Equals(senderGuid, userGuid1FromDB) && string.Equals(receiverGuid, userGuid2FromDB)) ||
                (string.Equals(senderGuid, userGuid2FromDB) && string.Equals(receiverGuid, userGuid1FromDB)))
            {
                JObject messageToDeliverJobj = new JObject();
                messageToDeliverJobj.Add(FlakeConstant.Message.SenderUserGuid.ToString(), senderGuid);
                messageToDeliverJobj.Add(FlakeConstant.Message.SenderUserName.ToString(), senderName);
                messageToDeliverJobj.Add(FlakeConstant.Message.FlakeChatGuid.ToString(), flakeChatJobj[FlakeConstant.FlakeChat.FlakeChatGuid.ToString()]);
                messageToDeliverJobj.Add(FlakeConstant.Message.SendUtcTime.ToString(), utcTime);
                messageToDeliverJobj.Add(FlakeConstant.Message.MessageBody.ToString(), message);

                string resultMessage = messageToDeliverJobj.ToString();
                await db.ListRightPushAsync(flakeChatJobj[FlakeConstant.FlakeChat.FlakeChatGuid.ToString()].ToString(), resultMessage);
                await db.ListRightPushAsync(FlakeConstant.UserNotificaitonQueuePrefix+receiverGuid, resultMessage);

                JToken resultStatus = new JObject(new JProperty("Status", "Success"));

                //Push notification to APN Server
                JObject alert = new JObject();
                alert.Add("alert", "New Message.");
                JObject payload = new JObject();
                payload.Add("Aps", alert);
                JObject notification = new JObject();
                notification.Add("Payload", payload);

                string receiverJson = await db.StringGetAsync(receiverGuid.ToString());
                JObject receiverJobj = JObject.Parse(receiverJson);

                notification.Add(FlakeConstant.User.DeviceToken.ToString(), receiverJobj[FlakeConstant.User.DeviceToken.ToString()]);
                AppleNotification.SendNotification(notification);
                
                return resultStatus;
            }
            else
            {
                throw new Exception("Invalid sender and/or receiver value at SendFlakeChatMessage");
            }
        }

        //TODO TODO TODO
        //Get Contact List

        //TODO TODO TODO
        //Add Contact to Contact List
    }
}
