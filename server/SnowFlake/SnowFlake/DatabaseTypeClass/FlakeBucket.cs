﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnowFlake.DatabaseTypeClass
{
    public class FlakeBucket
    {
        public string LocationBucket
        {
            get; set;
        }

        public List<MiniFlake> MiniFlakeList
        {
            get; set;
        }
    }
}
