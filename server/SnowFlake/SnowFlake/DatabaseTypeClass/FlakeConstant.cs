﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnowFlake.DatabaseTypeClass
{
    class FlakeConstant
    {
        public enum MiniFlake
        {
            FlakeGuid = 0,
            UserGuid = 1,
            UserName = 2,
            Latitude = 3,
            Longitude = 4,
            CreateTimeStamp = 5,
            QuestionGuid = 6,
            ReceiverGuid = 7
        }

        public enum MiniUser
        {
            UserGuid = 0,
            UserName = 1
        }

        public enum MiniFlakeChat
        {
            FlakeChatGuid = 0,
            FlakeGuid = 1,
            UserGuid = 2,
            UserName = 3,
            JoinUserGuid = 4,
            JoinUserName = 5
        }

        public enum Question
        {
            QuestionGuid = 0,
            QuestionText = 1,
            Answer1 = 2,
            Answer2 = 3,
            Answer3 = 4
        }

        public enum Flake
        {
            FlakeGuid = 0,
            UserGuid = 1,
            UserName = 2,
            Latitude = 3,
            Longitude = 4,
            CreateTimeStamp = 5,
            QuestionGuid = 6,
            QuestionCorrectAnswer = 7,
            OpenCount = 8,
            ReceiverGuid = 9,
            Content = 10,
            FlakeListGuid = 11,
            FlakeChatListGuid = 12,
            ContactListGuid = 13,
            Gender = 14
        }

        public enum FlakeChat
        {
            FlakeChatGuid = 0,
            FlakeGuid = 1,
            UserGuid = 2,
            UserName = 3,
            JoinUserGuid = 4,
            JoinUserName = 5,
            Content = 6,
            Chat = 7
        }

        public enum FlakeBucket
        {
            LocationBucket = 0,
            MiniFlakeList = 1
        }

        public enum User
        {
            UserGuid = 0,
            UserName = 1,
            UserPassword = 2,
            CreateTimeStamp = 3,
            ContactListGuid = 4,
            FlakeListGuid = 5,
            FlakeChatListGuid = 6,
            DeviceToken = 7
        }

        public enum ContactList
        {
            ContactListGuid = 0,
            ContactList = 1
        }

        public enum FlakeList
        {
            FlakeListGuid = 0,
            FlakeList = 1
        }

        public enum FlakeChatList
        {
            FlakeChatListGuid = 0,
            FlakeChat = 1
        }

        public enum Message
        {
            SenderUserGuid = 0,
            SenderUserName = 1,
            SendUtcTime = 2,
            FlakeChatGuid = 3,
            MessageBody = 4
        }

        public const string ContactListPrefix = "UserContactList_";

        public const string FlakePrefix = "Flake_";

        public const string FlakeChatPrefix = "FlakeChar_";

        public const string FlakeBucketPrefix = "FlakeBucket_";

        public const string FlakeListPrefix = "UserFlakeList_";

        public const string FlakeChatListPrefix = "UserFlakeChatList_";
        
        public const string MiniFlakePrefix = "MiniFlake_";

        public const string MiniUserPrefix = "MiniUser_";

        public const string MiniFlakeChatPrefix = "MiniFlakeChat_";

        public const string QuestionPrefix = "Question_";

        public const string UserPrefix = "User_";

        public const string LocationBucketPrefix = "LocationBucket_";

        public const string UserTriedFlakePrefix = "UserTriedFlakePrefix_";

        public const string UserNotificaitonQueuePrefix = "UserNotificationQueuePrefix_";

        public FlakeConstant()
        {
        }
    }
}
