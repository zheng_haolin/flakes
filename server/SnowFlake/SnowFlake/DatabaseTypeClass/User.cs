﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnowFlake.DatabaseTypeClass
{
    public class User
    {
        public string UserGuid
        {
            get; set;
        }

        public string UserName
        {
            get; set;
        }

        public string UserPassword
        {
            get; set;
        }

        public string DeviceToken
        {
            get; set;
        }

        public string CreateTimeStamp
        {
            get; set;
        }

        public List<MiniUser> ContactList
        {
            get; set;
        }

        public List<MiniFlake> FlakeList
        {
            get; set;
        }

        public List<MiniFlakeChat> FlakeContent
        {
            get; set;
        }

        public string Gender
        {
            get; set;
        }

        public User()
        {
        }
    }
}
