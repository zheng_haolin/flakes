﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnowFlake.DatabaseTypeClass
{
    public class MiniUser
    {
        public string UserGuid
        {
            get; set;
        }

        public string UserName
        {
            get; set;
        }

        public MiniUser()
        {
        }
    }
}
