﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnowFlake.DatabaseTypeClass
{
    class Question
    {
        public string QuestionGuid
        {
            get; set;
        }

        public string QuestionText
        {
            get; set;
        }

        public string Answer1
        {
            get; set;
        }

        public string Answer2
        {
            get; set;
        }

        public string Answer3
        {
            get; set;
        }

        public Question()
        {

        }
    }
}
