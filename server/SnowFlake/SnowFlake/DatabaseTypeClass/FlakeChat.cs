﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnowFlake.DatabaseTypeClass
{
    public class FlakeChat
    {
        public string FlakeChatGuid
        {
            get; set;
        }

        public string FlakeGuid
        {
            get; set;
        }

        public string UserGuid
        {
            get; set;
        }

        public string UserName
        {
            get; set;
        }

        public string JoinUserGuid
        {
            get; set;
        }

        public string JoinUserName
        {
            get; set;
        }

        public string Content
        {
            get; set;
        }

        public List<string> Chat
        {
            get; set;
        }

        public FlakeChat()
        {
        }
    }
}
