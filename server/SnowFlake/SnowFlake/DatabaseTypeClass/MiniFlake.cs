﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnowFlake.DatabaseTypeClass
{
    public class MiniFlake
    {
        public string FlakeGuid
        {
            get; set;
        }

        public string UserGuid
        {
            get; set;
        }

        public string UserName
        {
            get; set;
        }

        public string Latitude
        {
            get; set;
        }

        public string Longitude
        {
            get; set;
        }

        public string CreateTimeStamp
        {
            get; set;
        }

        public string QuestionGuid
        {
            get; set;
        }

        public string ReceiverGuid
        {
            get; set;
        }

        public MiniFlake()
        {
        }
    }
}
