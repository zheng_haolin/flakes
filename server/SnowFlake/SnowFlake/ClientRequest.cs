﻿using Newtonsoft.Json.Linq;
using System;
using System.Threading.Tasks;

namespace SnowFlake
{
    /// <summary>
    /// Base class containing all client request handlers
    /// </summary>
    public class ClientRequest
    {
        /// <summary>
        /// Indicates request type when server receives a request from client.
        /// </summary>
        public enum RequestTypes
        {
            NewFlake = 0,
            OpenFlake = 1,
            DeleteFlake = 2,
            Push = 3,
            Chat = 4,
            AddFlaker = 5,
            Signup = 6,
            SearchForFlake = 7
        }

        public ClientRequest()
        {
        }

        public static async Task<JToken> parseClientRequestByRequestType(JObject json)
        {
            // Getting requestType from the JObject
            JToken token;
            json.TryGetValue("RequestType", out token);
            int requestType = Int32.Parse(token.ToString());
            Task<JToken> task = null;
            JToken result = null;

            try
            {
                Console.WriteLine();
                Console.WriteLine();
                switch (requestType)
                {
                    case 1:
                        Console.WriteLine("Case NewFlake");
                        task = RequestProcessor.NewFlake(json);
                        break;
                    case 2:
                        Console.WriteLine("Get FlakeChat");
                        task = RequestProcessor.GetFlakeChatListAsOwner(json);
                        break;
                    case 3:
                        Console.WriteLine("Get Flakes as owner");
                        task = RequestProcessor.GetFlakesAsOwner(json);
                        break;
                    case 4:
                        Console.WriteLine("Update FlakeChat");
                        break;
                    case 5:
                        Console.WriteLine("New Account ");
                        task = RequestProcessor.NewAccount(json);
                        break;
                    case 6:
                        Console.WriteLine("Get Flake Buckets");
                        task = RequestProcessor.GetFlakeBuckets(json);
                        break;
                    case 7:
                        Console.WriteLine("Get FlakeChat Message");
                        break;
                    case 8:
                        Console.WriteLine("Unlock/Pickup Flake to create FlakeChat.");
                        task = RequestProcessor.OpenFlake(json);
                        break;
                    case 9:
                        Console.WriteLine("Update Flaker Device");
                        break;
                    case 11:
                        Console.WriteLine("SignIn");
                        task = RequestProcessor.SignIn(json);
                        break;
                    case 12:
                        Console.WriteLine("GetQuestion");
                        task = RequestProcessor.GetQuestion(json);
                        break;
                    default:
                        Console.WriteLine("Default case");
                        break;
                }
            }
            catch (Exception)
            {

            }

            result = await task;
            return result;
        }
    }
}