﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using StackExchange.Redis;
using Newtonsoft.Json;
using SnowFlake.DatabaseTypeClass;

namespace SnowFlake
{
    class Program
    {
        static void Main(string[] args)
        {
            WebServer webserver = new WebServer(8080);
            webserver.Start();
            Console.WriteLine("Press enter to exit...");
            Console.ReadLine();

            /*
            //Testing JsonConvert.DeserializeObject
            string s = @"{'UserGuid': '12345','UserName': 'harley', 'Sthis': 'sdfsdf'}";
            MiniUser miniuser = JsonConvert.DeserializeObject<MiniUser>(s);
            */

            //Testing send notification 

            //AppleNotification.SendNotification("2ce2b8723bd3a40a25357a8e110fbcaa0f1891249c327d7e01d5014566257743");
        }
    }
}
