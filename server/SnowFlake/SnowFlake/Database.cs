﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StackExchange.Redis;

namespace SnowFlake
{
    class Database
    {
        //redis instance then share across process
        private static ConnectionMultiplexer redis;

        //empty Constructor
        public Database()
        {
        }

        /// <summary>
        /// Get redis instance in order to  interact with redis database
        /// </summary>
        /// <returns>redis instance</returns>
        public static ConnectionMultiplexer GetRedisInstance() {
            if (redis == null) {
                redis = ConnectionMultiplexer.Connect("127.0.0.1, password = foobared");
            }

            return redis;
        }
    }
}
