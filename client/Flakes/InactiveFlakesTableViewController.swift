//
//  InactiveFlakesTableViewController.swift
//  Flakes
//
//  Created by juan kou on 7/23/16.
//  Copyright © 2016 Flakes.com. All rights reserved.
//

import UIKit
import ENSwiftSideMenu
import AlamofireImage
import SwiftyJSON
import NYTPhotoViewer

class InactiveFlakesTableViewController: UITableViewController, ENSideMenuDelegate {
    
    let flakesSyncEngine = FlakesSyncEngine()
    var flakes = [Flake]()
    let loadingView = LoadingView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.sideMenuController()?.sideMenu?.delegate = self
        
        self.tableView.registerClass(InActiveFlakeTableViewCell.classForCoder(), forCellReuseIdentifier: "Cell")
        //self.flakes = getMiniFlakes()
    }
    
    override func viewWillAppear(animated: Bool) {
        
        super.viewWillAppear(animated)
        
        loadingView.showCover()
        
        self.flakesSyncEngine.getInactiveFlakes() {
            (result:Bool, json:JSON?) in
            if (result) {
                self.saveInactiveFlakes(json)
                self.loadingView.hide()
            } else {
                self.loadingView.hide()
            }
        }
    }
    
    @IBAction func toggleSideMenu(sender: AnyObject) {
        toggleSideMenuView()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func saveInactiveFlakes(json:JSON?) {
        var newflakes = [Flake]()
        
        let miniflakes = json!["FlakeList"]
        print("MINIFLAKE: ")
        print(miniflakes)
        
        for flake in miniflakes{
            let miniflake = flake.1
            
            let temp = Flake(flakeGuid: miniflake["FlakeGuid"].string!,
                             userGuid: miniflake["UserGuid"].string!,
                             userName: miniflake["UserName"].string!,
                             latitude: Double(miniflake["Latitude"].string!)!,
                             longitude: Double(miniflake["Longitude"].string!)!,
                             createTimeStamp: miniflake["CreateTimeStamp"].string!,
                             questionGuid: miniflake["QuestionGuid"].string,
                             receiverGuid: miniflake["QuestionCorrectAnswer"].string
            )
            
            newflakes.append(temp)
        }
    
        self.flakes = newflakes
        print(self.flakes)
        
        tableView.reloadData()
    }
    
    func zoomImage(img: UIImage) {
        let photo = Photo(img: img)
        let photos = [photo]
        let zoomVC = NYTPhotosViewController(photos: photos)
        self.presentViewController(zoomVC, animated: true, completion: nil)
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return flakes.count
    }
    
    
     override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! InActiveFlakeTableViewCell
     
        // Configure the cell...
        let flake = self.flakes[indexPath.row]
        // Configure the cell...
        
        let width:Int = Int(FlakesConstant.screenWidth - 10 * 2)
        let height = 210
        let url = MapManager.getGoogleMapImageByLocation(flake.longitude, latitude: flake.latitude, width: width, height: height)
        cell.map.af_setImageWithURL(url)
        //cell.userLocation.text = String(flake.longitude) + " " + String(flake.latitude)
     
        return cell
     }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let currentCell = tableView.cellForRowAtIndexPath(indexPath) as! InActiveFlakeTableViewCell
        zoomImage(currentCell.map.image!)
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 220
    }
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
     if editingStyle == .Delete {
     // Delete the row from the data source
     tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
     } else if editingStyle == .Insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
       // MARK: - ENSideMenu Delegate
    func sideMenuWillOpen() {
        print("sideMenuWillOpen")
    }
    
    func sideMenuWillClose() {
        print("sideMenuWillClose")
    }
    
    func sideMenuShouldOpenSideMenu() -> Bool {
        print("sideMenuShouldOpenSideMenu")
        return true
    }
    
    func sideMenuDidClose() {
        print("sideMenuDidClose")
    }
    
    func sideMenuDidOpen() {
        print("sideMenuDidOpen")
    }

    
}
