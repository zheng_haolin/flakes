//
//  RequestType.swift
//  Flakes
//
//  Created by Kelvin Deng on 7/23/16.
//  Copyright © 2016 Flakes.com. All rights reserved.
//

import Foundation

class RequestType: NSObject {
    static let CreateFlake = "1"
    static let GetFlakeChat = "2"
    static let GetFlakeAsOwner = "3"
    static let UpdateFlakeChat = "4"
    static let Signup = "5"
    static let GetFlakeBuckets = "6"
    static let GetFlakeChatMessage = "7"
    static let UnlockFlakeAndCreateFlakeChat = "8"
    static let UpdateFlakerDevice = "9"
    static let AddFlakerContact = "10"
    static let Signin = "11"
    static let GetQuestion = "12"
}