//
//  CompassViewController.swift
//  Flakes
//
//  Created by DuRuitao on 7/22/16.
//  Copyright © 2016 Flakes.com. All rights reserved.
//

import UIKit
import CoreLocation
import SwiftyJSON
import NYTPhotoViewer

class CompassViewController: UIViewController, CLLocationManagerDelegate, iCarouselDelegate, iCarouselDataSource, UIGestureRecognizerDelegate{
    
    // requried Properties
    let token:String = "random"
    let userGuid:String = "tobytoyuito@gmai.com"
    let userName:String = "tobytoyuito"
    
    // Properties
    let compassPointer:UIImageView = UIImageView()
    let compassCenter:UIImageView = UIImageView()
    let screenWidth:CGFloat = UIScreen.mainScreen().bounds.size.width
    let screenHeight:CGFloat = UIScreen.mainScreen().bounds.size.height
    let distanceOfFlake:UILabel = UILabel()
    var flakeView:UIView = UIView()
    
    let loadingView = LoadingView()
    
    var tempView = UIView()
    var distance:Double = 10000
    var first = true
    var gettingBuckets = false
    
    let alertDistance:Double = 5
    let alertNoLocation:UIAlertView = UIAlertView(title: "No Location Data", message: "Please enable location service", delegate: nil, cancelButtonTitle: "Close App")
    
    // Question Property
    var questionSubview:UIView = UIView()
    var questionLabel:UILabel = UILabel()
    var answer1Button:UIButton = UIButton()
    var answer2Button:UIButton = UIButton()
    var answer3Button:UIButton = UIButton()
    var answer4Button:UIButton = UIButton()
    var validateLabel:UILabel = UILabel()
    var searchOthers:UIButton = UIButton()
    
    // Flake Property
    var flakeContentSubview:UIView = UIView()
    var flakeContent:UILabel = UILabel()
    var flakeContentLabel:UILabel = UILabel()
    var goToChatButton:UIButton = UIButton()
    var exploreMore:UIButton = UIButton()
    
    var flakes = [Flake]()
    let carouselView = iCarousel()
    
    
    var currentFlake:Flake? = nil
    var currentLocation:CLLocation? = nil
    var preLocation:CLLocation? = nil
    //var bearingToFlake:CLLocationDirection? = nil
    var bearingToFlake:CLLocationDirection? = 0//90.1689437644401
    var currentHeading:CLLocationDirection? = nil
    
    let locationManager:CLLocationManager = CLLocationManager()
    
    let flakesSyncEngine = FlakesSyncEngine()
    
    var timer:NSTimer?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingHeading()
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.startUpdatingLocation()
        
        carouselView.delegate = self
        carouselView.dataSource = self
        carouselView.type = .CoverFlow
        
        // UI
        setCompassPointer()
        //setCompassCenter()
        setCarouselView()
        setDistanceOfFlake()
        setFlakeView()
        
        print("authorization number: \(CLLocationManager.authorizationStatus().rawValue)")
        
        if CLLocationManager.authorizationStatus() != .AuthorizedWhenInUse{
            //alertNoLocation.show()
        }
        
        self.timer = NSTimer.scheduledTimerWithTimeInterval(10, target: self, selector: #selector(CompassViewController.update), userInfo: nil, repeats: true)
    }
    
    override func viewDidAppear(animated: Bool) {
        //loadingView.show()
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        //self.timer?.invalidate()
    }
    
    func waitForLocationAndHeading(){
        if (currentLocation == nil) || (currentHeading == nil){
            //self.loadingView.show()
            while (currentLocation == nil) || (currentHeading == nil){
                print("getting \(currentLocation) \(currentHeading)")
            }
            //self.loadingView.hide()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setCompassPointer() {
        
        let width:CGFloat = 200
        let height:CGFloat = 200//259
        let x = (screenWidth - width) / 2
        let y = (screenHeight - height) * 2 / 7
        
        compassPointer.frame = CGRectMake(x, y, width, height)
        
        compassPointer.image = UIImage(named: "compassBig")
        
        //compassPointer.layer.anchorPoint = CGPointMake(0.5, 0.5 - 33.5 / 2 / height);
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(CompassViewController.tapped))
        singleTap.numberOfTapsRequired = 1
        compassPointer.addGestureRecognizer(singleTap)
        compassPointer.userInteractionEnabled = true
        
        self.view.addSubview(compassPointer)
    }
    
    func setCompassCenter() {
        
        let width:CGFloat = 194
        let height:CGFloat = 194
        let x:CGFloat = 33
        let y:CGFloat = 33.5
        
        compassCenter.frame = CGRectMake(x, y, width, height)
        compassCenter.image = UIImage(named: "compass-center")
        
        self.compassPointer.addSubview(compassCenter)
    }
    
    func setCarouselView() {
        
        let width:CGFloat = 259
        let height:CGFloat = 220
        let x = (screenWidth - width) / 2
        let y = (screenHeight - height) * 8 / 10
        
        carouselView.frame = CGRectMake(x, y, width, height)
        self.view.addSubview(carouselView)
    }
    
    func setDistanceOfFlake() {
        let width:CGFloat = 150
        let height:CGFloat = 50
        let x:CGFloat = (screenWidth - width) / 2
        let y:CGFloat = 40
        
        let animation: CATransition = CATransition()
        animation.duration = 1.0
        animation.type = kCATransitionFade
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        distanceOfFlake.layer.addAnimation(animation, forKey: "changeTextTransition")
        
        distanceOfFlake.frame = CGRectMake(x, y, width, height)
        distanceOfFlake.text = "2000M"
        distanceOfFlake.font = UIFont.FlakesFont(size: 23)
        distanceOfFlake.textAlignment = NSTextAlignment.Center
        distanceOfFlake.textColor = UIColor.whiteColor()
        
        distanceOfFlake.backgroundColor = UIColor.LightBlue
        distanceOfFlake.layer.cornerRadius = 5
        distanceOfFlake.clipsToBounds = true
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(CompassViewController.tapped))
        singleTap.numberOfTapsRequired = 1
        distanceOfFlake.addGestureRecognizer(singleTap)
        distanceOfFlake.userInteractionEnabled = true
        
        self.view.addSubview(distanceOfFlake)
    }
    
    func setFlakeView() {
        
        flakeView.alpha =  0
        
        let width:CGFloat = screenWidth - 30
        let height:CGFloat = screenHeight - 100
        let x = (screenWidth - width) / 2
        let y = (screenHeight - height) / 2 - 20
        
        flakeView.frame = CGRectMake(x, y, width, height)
        flakeView.backgroundColor = UIColor.LightBlue
        flakeView.layer.cornerRadius = 10
        
        let offset:CGFloat = 25
        let widthOfComponents = width - offset * 2
        
        // set up question subview
        let questionHeight:CGFloat = 200
        let answerHeight:CGFloat = 40
        
        questionSubview.frame = CGRectMake(0, 0, width, height)
        
        questionLabel.frame = CGRectMake(offset, 15, widthOfComponents, questionHeight)
        answer1Button.frame = CGRectMake(offset, 2*15+questionHeight, widthOfComponents, answerHeight)
        answer2Button.frame = CGRectMake(offset, 3*15+questionHeight+answerHeight, widthOfComponents, answerHeight)
        answer3Button.frame = CGRectMake(offset, 4*15+questionHeight+answerHeight*2, widthOfComponents, answerHeight)
        answer4Button.frame = CGRectMake(offset, 5*15+questionHeight+answerHeight*3, widthOfComponents, answerHeight)
        validateLabel.frame = CGRectMake(offset, 6*15+questionHeight+answerHeight*4, widthOfComponents, answerHeight)
        searchOthers.frame = CGRectMake(offset, height-offset-40, widthOfComponents, 40)
        
        questionLabel.textAlignment = .Center
        questionLabel.font = UIFont.FlakesFont(size: 23)
        questionLabel.textColor = UIColor.whiteColor()
        questionLabel.numberOfLines = 0
        questionLabel.lineBreakMode = .ByWordWrapping
        
        answer1Button.titleLabel!.textAlignment = .Center
        answer1Button.titleLabel!.font = UIFont.FlakesFont(size: 20)
        answer1Button.titleLabel!.textColor = UIColor.whiteColor()
        answer1Button.backgroundColor = UIColor.LightBlue
        answer1Button.layer.cornerRadius = 5
        answer1Button.layer.borderColor = UIColor.whiteColor().CGColor
        answer1Button.layer.borderWidth = 1
        
        answer2Button.titleLabel!.textAlignment = .Center
        answer2Button.titleLabel!.font = UIFont.FlakesFont(size: 20)
        answer2Button.titleLabel!.textColor = UIColor.whiteColor()
        answer2Button.backgroundColor = UIColor.LightBlue
        answer2Button.layer.cornerRadius = 5
        answer2Button.layer.borderColor = UIColor.whiteColor().CGColor
        answer2Button.layer.borderWidth = 1

        answer3Button.titleLabel!.textAlignment = .Center
        answer3Button.titleLabel!.font = UIFont.FlakesFont(size: 20)
        answer3Button.titleLabel!.textColor = UIColor.whiteColor()
        answer3Button.backgroundColor = UIColor.LightBlue
        answer3Button.layer.cornerRadius = 5
        answer3Button.layer.borderColor = UIColor.whiteColor().CGColor
        answer3Button.layer.borderWidth = 1
        
        answer1Button.setTitleColor(UIColor.LightBlue, forState: .Highlighted)
        answer2Button.setTitleColor(UIColor.LightBlue, forState: .Highlighted)
        answer3Button.setTitleColor(UIColor.LightBlue, forState: .Highlighted)
        
        answer1Button.addTarget(self, action: #selector(highlightButton), forControlEvents: .TouchDown)
        answer2Button.addTarget(self, action: #selector(highlightButton), forControlEvents: .TouchDown)
        answer3Button.addTarget(self, action: #selector(highlightButton), forControlEvents: .TouchDown)
        
        validateLabel.textAlignment = .Center
        validateLabel.font = UIFont.FlakesFont(size: 23)
        validateLabel.textColor = UIColor.LightYellow
            
        questionSubview.addSubview(questionLabel)
        questionSubview.addSubview(answer1Button)
        questionSubview.addSubview(answer2Button)
        questionSubview.addSubview(answer3Button)
        questionSubview.addSubview(answer4Button)
        questionSubview.addSubview(validateLabel)
        questionSubview.addSubview(searchOthers)
        
        
        // set up content subview
        flakeContentSubview.frame = CGRectMake(0, 0, width, height)
        
        flakeContentLabel.frame = CGRectMake(offset, 15, widthOfComponents, 50)
        flakeContent.frame = CGRectMake(offset, 15*2+50, widthOfComponents, 200)
        goToChatButton.frame = CGRectMake(offset, height-offset-95, widthOfComponents, 40)
        exploreMore.frame = CGRectMake(offset, height-offset-40, widthOfComponents, 40)
        
        flakeContentLabel.text = "Message:"
        flakeContentLabel.textAlignment = .Center
        flakeContentLabel.font = UIFont.FlakesFont(size: 20)
        flakeContentLabel.textColor = UIColor.whiteColor()
        flakeContentLabel.numberOfLines = 0
        flakeContentLabel.lineBreakMode = .ByWordWrapping
        
        flakeContent.textAlignment = .Center
        flakeContent.font = UIFont.FlakesFont(size: 23)
        flakeContent.textColor = UIColor.whiteColor()
        flakeContent.numberOfLines = 0
        flakeContent.lineBreakMode = .ByWordWrapping
        
        goToChatButton.setTitle("Go To Chat", forState: UIControlState.Normal)
        goToChatButton.addTarget(self, action: #selector(CompassViewController.closeFlakeView), forControlEvents: UIControlEvents.TouchUpInside)
        goToChatButton.titleLabel!.textAlignment = .Center
        goToChatButton.titleLabel!.font = UIFont.FlakesFont(size: 18)
        goToChatButton.setTitleColor(UIColor.LightBlue, forState: .Normal)
        goToChatButton.backgroundColor = UIColor.whiteColor()
        goToChatButton.layer.cornerRadius = 5
        goToChatButton.layer.borderColor = UIColor.whiteColor().CGColor
        goToChatButton.layer.borderWidth = 1
        goToChatButton.addTarget(self, action: #selector(CompassViewController.goToChat), forControlEvents: UIControlEvents.TouchUpInside)
        goToChatButton.addTarget(self, action: #selector(CompassViewController.highlightButtonReverse), forControlEvents: UIControlEvents.TouchDown)
        goToChatButton.setTitleColor(UIColor.whiteColor(), forState: .Highlighted)
        
        exploreMore.setTitle("Explore More Flakes", forState: UIControlState.Normal)
        exploreMore.addTarget(self, action: #selector(CompassViewController.closeFlakeView), forControlEvents: UIControlEvents.TouchUpInside)
        exploreMore.titleLabel!.textAlignment = .Center
        exploreMore.titleLabel!.font = UIFont.FlakesFont(size: 18)
        exploreMore.setTitleColor(UIColor.LightBlue, forState: .Normal)
        exploreMore.backgroundColor = UIColor.whiteColor()
        exploreMore.layer.cornerRadius = 5
        exploreMore.layer.borderColor = UIColor.whiteColor().CGColor
        exploreMore.layer.borderWidth = 1
        exploreMore.addTarget(self, action: #selector(CompassViewController.closeFlakeView), forControlEvents: UIControlEvents.TouchUpInside)
        
        flakeContentSubview.addSubview(flakeContentLabel)
        flakeContentSubview.addSubview(flakeContent)
        flakeContentSubview.addSubview(goToChatButton)
        flakeContentSubview.addSubview(exploreMore)
        
        flakeView.addSubview(questionSubview)
        flakeView.addSubview(flakeContentSubview)
        
        self.view.addSubview(flakeView)
        
        
    }
    
    func highlightButtonReverse(sender:UIButton) {
        sender.backgroundColor = UIColor.LightBlue
        sender.layer.borderColor = UIColor.whiteColor().CGColor
        sender.layer.borderWidth = 1
    }
    
    func highlightButton(sender:UIButton) {
        sender.backgroundColor = UIColor.whiteColor()
    }
    
    func rotateCompassPointer(bearing: CGFloat, heading: CGFloat){
        let degrees:CGFloat = bearing - heading
        //print("bearing:\(bearing) heading:\(heading)")
        compassPointer.transform = CGAffineTransformMakeRotation(degrees * CGFloat(M_PI)/180);
    }
    
    func locationManager(manager: CLLocationManager, didUpdateHeading heading: CLHeading) {
        currentHeading = heading.magneticHeading
        
        if (currentFlake != nil) && (bearingToFlake != nil){
            rotateCompassPointer(CGFloat(bearingToFlake!), heading: CGFloat(currentHeading!))
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        preLocation = currentLocation
        
        currentLocation = manager.location!
        //print("locations = \(currentLocation?.coordinate.latitude) \(currentLocation?.coordinate.longitude)")
        
        if (preLocation != nil){
            print("\(preLocation!.distanceFromLocation(currentLocation!))")
        }
        
        
        if (currentLocation != nil){// && (currentHeading != nil) {
            if first || ((preLocation != nil) && (preLocation!.distanceFromLocation(currentLocation!) > 5)){
                if !gettingBuckets{
                    gettingBuckets = true
                    getMiniFlakes()
                    gettingBuckets = false
                }
                if (carouselView.currentItemIndex != -1){
                    currentFlake = flakes[carouselView.currentItemIndex]
                }
                first = false
            }
        }
        
        if (currentFlake != nil) && (currentLocation != nil) && (currentHeading != nil){
            setUpVariable()
        }
    }
    
    func setUpVariable(){
        
        if (currentLocation != nil) && (flakes.count>0){
            let to_coor = CLLocation(latitude: currentFlake!.latitude, longitude: currentFlake!.longitude)
            distance = currentLocation!.distanceFromLocation(to_coor)
            bearingToFlake = bearingBetweenLocation(currentLocation!, l2:to_coor)
            rotateCompassPointer(CGFloat(bearingToFlake!), heading: CGFloat(currentHeading!))
            distanceOfFlake.text = "\(String(Int(distance)))M"
            if distance < alertDistance{
                //distanceOfFlake.textColor = UIColor.Green
                distanceOfFlake.backgroundColor = UIColor.Green
                distanceOfFlake.text = "PICK UP"
            } else {
                distanceOfFlake.backgroundColor = UIColor.LightBlue
            }
        }
        
        
    }
    
    func numberOfItemsInCarousel(carousel: iCarousel) -> Int {
        return flakes.count
    }
    
    func carousel(carousel: iCarousel, viewForItemAtIndex index: Int, reusingView view: UIView?) -> UIView {
        var cardView = UIView(frame: CGRect(x: 0, y: 0, width: FlakesConstant.screenWidth-130, height: (FlakesConstant.screenWidth-130)/320*250))
        cardView.layer.cornerRadius = 20
        cardView.layer.borderColor = UIColor.LightBlue.CGColor
        cardView.layer.borderWidth = 1
        
        
        let flake:Flake = flakes[index]
        let address = (MapManager.getGoogleReverseGeocoding(flake.longitude, latitude: flake.latitude))
        let url = MapManager.getGoogleMapImageByLocation(flake.longitude, latitude: flake.latitude)
        
        let back = UIImageView(frame: CGRectMake(0, 0, FlakesConstant.screenWidth-130, (FlakesConstant.screenWidth-130)/320*250))
        
        back.af_setImageWithURL(url)
        back.layer.cornerRadius = 20
        back.layer.borderWidth = 1
        back.clipsToBounds = true

        
        cardView.addSubview(back)
        cardView.userInteractionEnabled = true
        cardView.clipsToBounds = true
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(CompassViewController.tapped))
        singleTap.numberOfTapsRequired = 1
        
        cardView.addGestureRecognizer(singleTap)
        cardView.userInteractionEnabled = true

        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(CompassViewController.zoomImage))
        doubleTap.numberOfTapsRequired = 2
        
        cardView.addGestureRecognizer(doubleTap)
        cardView.userInteractionEnabled = true

        
        let addressView = UILabel(frame: CGRect(x: 0, y: 0, width: FlakesConstant.screenWidth-130, height: 20))
        addressView.text = address
        addressView.textAlignment = .Center
        addressView.font = UIFont.FlakesFont(size: 10)
        addressView.textColor = UIColor.whiteColor()
        addressView.numberOfLines = 0
        addressView.lineBreakMode = .ByWordWrapping
        addressView.backgroundColor = UIColor.LightBlue
        addressView.clipsToBounds = true
        
        cardView.addSubview(addressView)
        
        return cardView
    }
    
    func zoomImage() {
        
        if (currentFlake != nil) {
            let url = MapManager.getGoogleMapImageByLocation(currentFlake!.longitude, latitude: currentFlake!.latitude)
            let back = UIImageView(frame: CGRectMake(0, 0, FlakesConstant.screenWidth-130, (FlakesConstant.screenWidth-130)/320*250))
            
            back.af_setImageWithURL(url)
            back.layer.cornerRadius = 20
            back.layer.borderWidth = 1
            back.clipsToBounds = true

            
            let photo = Photo(img: back.image!)
            let photos = [photo]
            let zoomVC = NYTPhotosViewController(photos: photos)
            self.presentViewController(zoomVC, animated: true, completion: nil)
        }
    }

    
    func carouselCurrentItemIndexDidChange(carousel: iCarousel) {
        compassCenter.image = UIImage(named: "compass-center")
        if carouselView.currentItemIndex != -1{
            currentFlake = flakes[carouselView.currentItemIndex]
        }
        
        print(carouselView.currentItemIndex)
        
        if(currentHeading != nil) {
            setUpVariable()
        }
    }
    
    func carousel(carousel: iCarousel, didSelectItemAtIndex index: Int) {
        compassCenter.image = UIImage(named: "compass-center")
        currentFlake = flakes[index]
        print(index)
        
        if(currentHeading != nil) {
            setUpVariable()
        }
    }
    
    func carousel(carousel: iCarousel, valueForOption option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if option == iCarouselOption.Spacing{
            return value*1.2
        }
        return value
    }
    
    //( 47.645192322435918) Optional(-122.12215910001041)    47.645628, -122.122642
    //let to_coor:CLLocation = CLLocation(latitude: 47.645628, longitude: -122.122642)
    //let to_coor:CLLocation = CLLocation(latitude: 47.631495, longitude: -122.122391) //south 1.53km 0.666274004425901 -179.3354
    //let to_coor:CLLocation = CLLocation(latitude: 47.645088, longitude: -122.071636) // east 3.8km 90.1689437644401    90.2264112
    //let to_coor:CLLocation = CLLocation(latitude: 47.676230, longitude: -122.125828) // north 3.46km 175.4470  -4.554323
    //let to_coor:CLLocation = CLLocation(latitude: 47.645913, longitude: -122.194970) // west 5.43km 90.8523  -89.1413

    
    func getMiniFlakes(){
        print("called")
        
        if (DefaultManager.sharedInstance.getToken() == nil) {
            return
        }
        
        let param = getGetFlakeBucketsParam()
        
        print(param)
        flakesSyncEngine.getFlakeBuckets(param) {
         
            (result:Bool, json:JSON?) in
            
            if (result) {
                //flakes = []
                print("json")
                print(json!)
                
                var newflakes = [Flake]()
                
                let miniflakes = json!["MiniFlakes"]
                print("MINIFLAKE: ")
                print(miniflakes)
                
                for flake in miniflakes{
                    
                    let miniflake = flake.1
                    
                    let temp = Flake(flakeGuid: miniflake["FlakeGuid"].string!,
                                     userGuid: miniflake["UserGuid"].string!,
                                     userName: miniflake["UserName"].string!,
                                     latitude: Double(miniflake["Latitude"].string!)!,
                                     longitude: Double(miniflake["Longitude"].string!)!,
                                     createTimeStamp: miniflake["CreateTimeStamp"].string!,
                                     questionGuid: miniflake["QuestionGuid"].string,
                                     receiverGuid: miniflake["QuestionCorrectAnswer"].string
                    )
                    
                    newflakes.append(temp)
                }

                
                
                
                if (self.carouselView.currentItemIndex != -1) && (newflakes.count>1){
                    let currentFlakeGuid = self.flakes[self.carouselView.currentItemIndex].flakeGuid
                    var findIndex = -1
                    for i in 0..<newflakes.count{
                        if newflakes[i].flakeGuid == currentFlakeGuid{
                            findIndex = i
                            break
                        }
                    }
                    print("find index \(findIndex) guid\(currentFlakeGuid)")
                    print(self.flakes[self.carouselView.currentItemIndex])
                    if findIndex != -1{
                        if self.carouselView.currentItemIndex >= newflakes.count{
                            if(newflakes.count-1 != findIndex){
                                swap(&newflakes[newflakes.count-1], &newflakes[findIndex])
                            }
                        }
                        else{
                            if(self.carouselView.currentItemIndex != findIndex){
                                swap(&newflakes[self.carouselView.currentItemIndex], &newflakes[findIndex])
                            }
                        }
                    }
                }
                
                self.flakes = newflakes
                self.carouselView.reloadData()
                
            }
            else{
                print("fail get mini flakes")
            }
        }

    }
    
    func update(){
        print("update")
        if (flakes.count > 0) && (currentHeading != nil) && (currentLocation != nil){
            getMiniFlakes()
        }
    }
    
    func getGetFlakeBucketsParam() -> AnyObject {
        
        let param:[String:AnyObject] = [
            "RequestType": RequestType.GetFlakeBuckets,
            "Token": DefaultManager.sharedInstance.getToken()!,
            "LocationList": [
                ["lat": Int(currentLocation!.coordinate.latitude),
                 "lon":Int(currentLocation!.coordinate.longitude)]
            ]
        ]

        return param
    }
    
    func getQuestion(questionGuid: String) {
        let param = getGetQuestionParam()
        
        print(param)
        flakesSyncEngine.getQuestion(param) {
            
            (result:Bool, json:JSON?) in
            
            if (result) {
                
                print("json")
                print(json!)
                
                
                
                let question = json!["Question"]["QuestionText"].string!
                self.questionLabel.text = question
                self.setupButton(self.answer1Button, answer: json!["Question"]["Answer1"].string!)
                self.setupButton(self.answer2Button, answer: json!["Question"]["Answer2"].string!)
                self.setupButton(self.answer3Button, answer: json!["Question"]["Answer3"].string!)
                self.validateLabel.text = ""
                
            }
            else{
                print("fail get mini flakes")
            }
        }

    }
    
    func getGetQuestionParam() -> AnyObject {
        

        let param:[String:String] = [
            "RequestType": RequestType.GetQuestion,
            "Token": DefaultManager.sharedInstance.getToken()!,
            "QuestionGuid": currentFlake!.questionGuid!
        ]
        
        return param
    }
    
    func getFlakeContent(answer:String){
        
        let param = getGetFlakeContentParam(answer)
        
        print(param)
        flakesSyncEngine.getContent(param) {
            
            (result:Bool, json:JSON?) in
            
            if (result) {
                if (self.currentFlake?.questionGuid == nil){
                    self.flakeContentLabel.text = "Unlocked Flake Message:"
                    var content:String? = json!["Content"].string
                    if content == nil{
                        content = "Empty"
                    }
                    self.flakeContent.text = content
                    
                    
                }
                else{
                    if (json!["OpenStatus"] != nil) {
                        
                        //Wrong answer
                        self.validateLabel.text = "😂 Sorry, wrong answer"
                        
                        self.searchOthers.setTitle("Explore More Flakes", forState: UIControlState.Normal)
                        self.searchOthers.addTarget(self, action: #selector(CompassViewController.closeFlakeView), forControlEvents: UIControlEvents.TouchUpInside)
                        self.searchOthers.titleLabel!.textAlignment = .Center
                        self.searchOthers.titleLabel!.font = UIFont.FlakesFont(size: 18)
                        self.searchOthers.setTitleColor(UIColor.LightBlue, forState: .Normal)
                        self.searchOthers.backgroundColor = UIColor.whiteColor()
                        self.searchOthers.layer.cornerRadius = 5
                        self.searchOthers.layer.borderColor = UIColor.whiteColor().CGColor
                        self.searchOthers.layer.borderWidth = 1
                        
                    } else {
                        
                        //get content
                        let content = json!["Content"].string!
                        self.onlyDisplayContent()
                        print("I choose \(answer)")
                        
                        self.flakeContentLabel.text = "You've pick up a new snowFlake!"
                        self.flakeContent.text = content
                        
                        UIView.transitionFromView(self.questionSubview, toView: self.flakeContentSubview, duration: 1, options: UIViewAnimationOptions.TransitionFlipFromRight, completion: nil)
                        
                    }
                }
            }
            else{
                print("fail get mini flakes")
            }
        }

    }
    
    func getGetFlakeContentParam(answer: String) -> AnyObject {
        
        let param:[String:AnyObject] = [
            "RequestType": RequestType.UnlockFlakeAndCreateFlakeChat,
            "Token": DefaultManager.sharedInstance.getToken()!,
            "Flake": [
                "QuestionCorrectAnswer": answer,
                "FlakeGuid": currentFlake!.flakeGuid
            ]
            
        ]
        
        return param
    }
    
    func onlyDisplayQuestion(){
        flakeContentSubview.alpha = 0
        questionSubview.alpha = 1
        self.flakeView.addSubview(questionSubview)
    }
    
    func onlyDisplayContent(){
        flakeContentSubview.alpha = 1
        questionSubview.alpha = 0
    }
    
    func tapped() {        
        if currentFlake == nil{
            return
        }
        let index = carouselView.currentItemIndex
        if distance < alertDistance{
            print("arrive tap")
            UIView.animateWithDuration(1, animations:  {() in
                self.flakeView.alpha = 1
            })
            
            if currentFlake!.questionGuid != nil{
                onlyDisplayQuestion()
                
                loadingView.showCover()
                
                getQuestion(currentFlake!.questionGuid!)
                
                loadingView.hide()
            }
            else{
                
                onlyDisplayContent()
                
                loadingView.showCover()
                
                getFlakeContent("")
                
                loadingView.hide()
            }
        }
        print("tapped \(index)")
    }
    
    func setupButton(button:UIButton, answer:String){
        button.setTitle(answer, forState: UIControlState.Normal)
        button.addTarget(self, action: #selector(CompassViewController.tappedAnswer), forControlEvents: UIControlEvents.TouchUpInside)
    }
    
    func tappedAnswer(sender:UIButton) {
        
        sender.backgroundColor = UIColor.LightBlue
        
        print("taapedanswer")
        let chosenAnswer = sender.titleLabel!.text
        //Get content if answer is correct
        
        var answerIndex = "-1"
        if( chosenAnswer == answer1Button.titleLabel!.text){
            answerIndex = "1"
        }
        else if( chosenAnswer == answer2Button.titleLabel!.text){
            answerIndex = "2"
        }
        else if( chosenAnswer == answer3Button.titleLabel!.text){
            answerIndex = "3"
        }
        
        
        loadingView.showCover()
        getFlakeContent(answerIndex)
        
        loadingView.hide()
    }
    
    func closeFlakeView(){
        UIView.animateWithDuration(1, animations:  {() in
            self.flakeView.alpha = 0
        })
        
        flakes.removeAtIndex(carouselView.currentItemIndex)
        carouselView.reloadData()
        if (carouselView.currentItemIndex == -1){
            currentFlake = nil
        }
        else{
            currentFlake = flakes[carouselView.currentItemIndex]
        }
    
        print("closeFlakeView")
    }
    
    func DegreesToRadians(degrees: Double ) -> Double {return degrees * M_PI / 180;};
    func RadiansToDegrees(radians: Double) -> Double {return radians * 180/M_PI;};

    func bearingBetweenLocation(l1: CLLocation , l2: CLLocation) -> Double{
        let lat1 = DegreesToRadians(l1.coordinate.latitude)
        let lon1 = DegreesToRadians(l1.coordinate.longitude)
        
        let lat2 = DegreesToRadians(l2.coordinate.latitude)
        let lon2 = DegreesToRadians(l2.coordinate.longitude)
        
        let dLon = lon2 - lon1
        
        let y = sin(dLon) * cos(lat2)
        let x = cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dLon)
        let radiansBearing = atan2(y, x)
    
        let degrees = RadiansToDegrees(radiansBearing)
        
        if degrees < 0{
            return degrees + 360
        }
        else{
            return degrees
        }
        
        //return degrees+180
    }
    
    func goToChat() {
        print("GoToChat")
        let chatView = ChatViewController();
        let navVC = UINavigationController(rootViewController: chatView)
        self.presentViewController(navVC, animated: true, completion: nil)
    }
    

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    class func createPublicFlakes() {
        //        let VC1 = CreateFlakeViewController(isPublic: true)
        //        let navController = UINavigationController(rootViewController: VC1) // Creating a navigation controller with VC1 at the root of the navigation stack.
        //        self.items!.presentViewController(navController, animated:true, completion: nil)
        print("Jane")
    }

}
