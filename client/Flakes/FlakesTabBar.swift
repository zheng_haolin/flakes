//
//  FlakesTabBar.swift
//  Flakes
//
//  Created by juan kou on 7/21/16.
//  Copyright © 2016 Flakes.com. All rights reserved.
//

import UIKit
import pop

class FlakesTabBar: UITabBar {
    
    var isActionPresent:Bool = false;
    var createFlakesButton:UIButton = UIButton()
    let selectionbgView = UIView()
    let hideView:UIView = UIView()
    let hideButton:UIButton = UIButton()
    let publicButton:UIButton = UIButton()
    let privateButton:UIButton = UIButton()
    let screenWidth = UIScreen.mainScreen().bounds.size.width
    let screenHeight = UIScreen.mainScreen().bounds.size.height
    let buttonWidth:CGFloat = 100
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        customize()
    }
    
    func customize() {
        self.backgroundColor = UIColor.redColor()
        self.translucent = false
        
        setUI()
        addFlakesTab()
        addCreateFlakeButton()
        addSelectionView()
        addCompassTab()
        addProfileTab()
        addContactsTab()
    }
    
    func setUI() {
        
        //disable the third tab bar item
        let item = self.items![2]
        item.title = ""
        item.enabled = false
            
        UITabBar.appearance().tintColor = UIColor.LightBlue
    }
    
    func addFlakesTab() {
        
        let item = self.items![1]
        item.image = UIImage(named: "snowflake-icon")!.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        item.selectedImage = UIImage(named: "snowflake-icon-hl")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        item.title = "SnowFlakes"
    }
    
    func addCompassTab() {
        
        let item = self.items![0]
        item.image = UIImage(named: "compass-1")!.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        item.selectedImage = UIImage(named: "compass-hl")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        item.title = "Compass"
    }
    
    func addProfileTab() {
        
        let item = self.items![4]
        item.image = UIImage(named: "profile")!.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        item.selectedImage = UIImage(named: "profile-hl")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        item.title = "Profile"
    }
    
    func addContactsTab() {
        
        let item = self.items![3]
        item.image = UIImage(named: "contacts")!.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        item.selectedImage = UIImage(named: "contacts-hl")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        item.title = "Contacts"
    }
    
    func addCreateFlakeButton() {
        
        let buttonImage:UIImage = UIImage(named: "addButton")!
        
        let leftPadding = screenWidth / 2 - 30
        let button:UIButton = UIButton(frame: CGRectMake(leftPadding, 5, 60, 40))
        button.setImage(buttonImage, forState: UIControlState.Normal)
        button.addTarget(self, action: #selector(FlakesTabBar.show), forControlEvents: UIControlEvents.TouchDown)
        
        self.addSubview(button)
        
    }
    
    func addSelectionView() {
        
        
        selectionbgView.frame = CGRectMake(0, 0, screenWidth, screenHeight)
       // selectionbgView.backgroundColor = UIColor.whiteColor()
        
        let visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .Light))
        
        visualEffectView.frame = selectionbgView.bounds
        selectionbgView.backgroundColor = UIColor.whiteColor()
        //selectionbgView.addSubview(visualEffectView)
        
        let padding = (screenWidth - 2 * buttonWidth) / 3
        var centerx = padding + buttonWidth / 2
        let centery = (screenHeight / 2 - buttonWidth) / 2 + screenHeight / 2 + 20
        
        //public button
        publicButton.frame = CGRectMake(centerx, centery, 0, 0)
        publicButton.setTitle("Public", forState: UIControlState.Normal)
        publicButton.titleLabel?.textColor = UIColor.whiteColor()
        publicButton.setImage(UIImage(named: "public"), forState: UIControlState.Normal)
        publicButton.addTarget(self, action: #selector(FlakesTabBar.createPubicFlakes), forControlEvents: UIControlEvents.TouchUpInside)
        
        centerx = centerx + padding + buttonWidth
        
        //private button
        privateButton.frame = CGRectMake(centerx, centery, 0, 0)
        privateButton.setTitle("Public", forState: UIControlState.Normal)
        privateButton.titleLabel?.textColor = UIColor.whiteColor()
        privateButton.setImage(UIImage(named: "private"), forState: UIControlState.Normal)
        privateButton.addTarget(self, action: #selector(self.createPrivateFlakes), forControlEvents: UIControlEvents.TouchUpInside)
        
        //hide view
        hideView.frame = CGRectMake(0, screenHeight - self.frame.size.height - 2, screenWidth, self.frame.size.height)
        
        //hide button
        hideButton.frame = CGRectMake(screenWidth / 2 - 30, 0, 60, 40)
        hideButton.setImage(UIImage(named: "cancel"), forState: UIControlState.Normal)
        hideButton.addTarget(self, action: #selector(FlakesTabBar.hide), forControlEvents: UIControlEvents.TouchUpInside)
        
        hideView.addSubview(hideButton)

    }
    
    func show() {
        
        if !isActionPresent {
            
            
            privateButton.alpha = 1
            publicButton.alpha = 1
            
            //show hidden view
            selectionbgView.layer.opacity = 1
            hideView.layer.opacity = 1
            
            let an1 = POPSpringAnimation(propertyNamed: kPOPLayerBounds)
            an1.toValue = NSValue(CGRect: CGRectMake(0, 0, buttonWidth, buttonWidth))
            publicButton.pop_addAnimation(an1, forKey: "size")
            
            let an2 = POPSpringAnimation(propertyNamed: kPOPLayerBounds)
            an2.toValue = NSValue(CGRect: CGRectMake(0, 0, buttonWidth, buttonWidth))
            privateButton.pop_addAnimation(an2, forKey: "size")
    
            UIApplication.sharedApplication().keyWindow?.rootViewController!.view.addSubview(selectionbgView)
            UIApplication.sharedApplication().keyWindow?.rootViewController!.view.addSubview(hideView)
            UIApplication.sharedApplication().keyWindow?.rootViewController!.view.addSubview(publicButton)
            UIApplication.sharedApplication().keyWindow?.rootViewController!.view.addSubview(privateButton)
            
            isActionPresent = true
            
        }
        
    }
    
    func hide()  {
        
        selectionbgView.layer.opacity = 0
        hideView.layer.opacity = 0
        privateButton.alpha = 0
        publicButton.alpha = 0
        
        let an1 = POPSpringAnimation(propertyNamed: kPOPViewBounds)
        an1.toValue = NSValue(CGRect: CGRectMake(0, 0, 0, 0))
        privateButton.pop_addAnimation(an1, forKey: "size")
        
        let an2 = POPSpringAnimation(propertyNamed: kPOPViewBounds)
        an2.toValue = NSValue(CGRect: CGRectMake(0, 0, 0, 0))
        publicButton.pop_addAnimation(an2, forKey: "size")
        
        isActionPresent = false
    }
    
    // Mark - TODO
    

    
    func createPrivateFlakes() {
        
        hide()
        
        var topVC = UIApplication.sharedApplication().keyWindow?.rootViewController
        while((topVC!.presentedViewController) != nil){
            topVC = topVC!.presentedViewController
        }
        
        let vc = CreateFlakeViewController(isPublic: false)
        topVC?.presentViewController(vc, animated: true, completion: nil)
    }
    
    func createPubicFlakes() {
        
        hide()
        
        var topVC = UIApplication.sharedApplication().keyWindow?.rootViewController
        while((topVC!.presentedViewController) != nil){
            topVC = topVC!.presentedViewController
        }
        
        let vc = CreateFlakeViewController(isPublic: true)
        topVC?.presentViewController(vc, animated: true, completion: nil)
    }

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
