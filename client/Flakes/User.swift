//
//  Flaker.swift
//  Flakes
//
//  Created by DuRuitao on 7/25/16.
//  Copyright © 2016 Flakes.com. All rights reserved.
//

import Foundation
import UIKit

class User {
    
    var userGuid:String!
    var userName:String!
    
    init(userGuid:String, userName:String) {
        
        self.userGuid = userGuid
        self.userName = userName
    }
}
