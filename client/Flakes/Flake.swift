//
//  Flake.swift
//  Flakes
//
//  Created by juan kou on 7/23/16.
//  Copyright © 2016 Flakes.com. All rights reserved.
//

import Foundation
import UIKit

class Flake {
    
    var flakeGuid:String
    var userGuid:String
    var userName:String
    var latitude:Double
    var longitude:Double
    var createTimeStamp:String
    var questionGuid:String?
    var questionCorrectAnswer:String?
    var content:String?
    var receiverGuid:String?
    
    init(flakeGuid:String, userGuid:String, userName:String, latitude:Double,longitude:Double, createTimeStamp:String, questionGuid:String?, receiverGuid: String?) {
        self.flakeGuid = flakeGuid
        self.userGuid = userGuid
        self.userName = userName
        self.latitude = latitude
        self.longitude = longitude
        self.createTimeStamp = createTimeStamp
        self.questionGuid = questionGuid
        self.receiverGuid = receiverGuid
    }
}
