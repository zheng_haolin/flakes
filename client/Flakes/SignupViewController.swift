//
//  SignupViewController.swift
//  Flakes
//
//  Created by Xing Gong on 7/26/16.
//  Copyright © 2016 Flakes.com. All rights reserved.
//

import UIKit

class SignupViewController: UIViewController {
    
    var userBox: UITextField = UITextField()
    var emailBox: UITextField = UITextField()
    var passwordBox: UITextField = UITextField()
    var passwordReenterBox: UITextField = UITextField()
    lazy var authManager:AuthenticationManager = AuthenticationManager()
    let loadingView:LoadingView = LoadingView()
    let errorLabel:UILabel = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI();
        // Do any additional setup after loading the view.
    }

    func setUI() {
        self.view.backgroundColor = UIColor.LightBlue
        
        userBox.frame = CGRectMake(FlakesConstant.screenWidth/10, FlakesConstant.screenHeight/3, FlakesConstant.screenWidth*8/10, 40)
        
        userBox.placeholder = "Enter your username here"
        userBox.font = UIFont.systemFontOfSize(15)
        userBox.borderStyle = UITextBorderStyle.RoundedRect
        
        userBox.returnKeyType = UIReturnKeyType.Done
        userBox.clearButtonMode = UITextFieldViewMode.WhileEditing;
        userBox.contentVerticalAlignment = UIControlContentVerticalAlignment.Center
        
        self.view.addSubview(userBox)
        
        
        emailBox.frame = CGRectMake(FlakesConstant.screenWidth/10, FlakesConstant.screenHeight/3+45, FlakesConstant.screenWidth*8/10, 40)
        
        emailBox.placeholder = "Enter your email here"
        emailBox.font = UIFont.systemFontOfSize(15)
        emailBox.borderStyle = UITextBorderStyle.RoundedRect
        
        emailBox.returnKeyType = UIReturnKeyType.Done
        emailBox.clearButtonMode = UITextFieldViewMode.WhileEditing;
        emailBox.contentVerticalAlignment = UIControlContentVerticalAlignment.Center
        
        self.view.addSubview(emailBox)
        
        
        passwordBox.frame = CGRectMake(FlakesConstant.screenWidth/10, FlakesConstant.screenHeight/3+90, FlakesConstant.screenWidth*8/10, 40)
        
        passwordBox.placeholder = "Enter your password here"
        passwordBox.font = UIFont.systemFontOfSize(15)
        passwordBox.borderStyle = UITextBorderStyle.RoundedRect
        passwordBox.secureTextEntry = true
        passwordBox.returnKeyType = UIReturnKeyType.Done
        passwordBox.clearButtonMode = UITextFieldViewMode.WhileEditing;
        passwordBox.contentVerticalAlignment = UIControlContentVerticalAlignment.Center
        
        self.view.addSubview(passwordBox)
        
        
        passwordReenterBox.frame = CGRectMake(FlakesConstant.screenWidth/10, FlakesConstant.screenHeight/3+135, FlakesConstant.screenWidth*8/10, 40)
        
        passwordReenterBox.placeholder = "Confirm your password again here"
        passwordReenterBox.font = UIFont.systemFontOfSize(15)
        passwordReenterBox.borderStyle = UITextBorderStyle.RoundedRect
        passwordReenterBox.secureTextEntry = true

        passwordReenterBox.returnKeyType = UIReturnKeyType.Done
        passwordReenterBox.clearButtonMode = UITextFieldViewMode.WhileEditing;
        passwordReenterBox.contentVerticalAlignment = UIControlContentVerticalAlignment.Center
        
        self.view.addSubview(passwordReenterBox)
        
        
        let loginButton = UIButton(frame: CGRectMake(FlakesConstant.screenWidth*11/20, FlakesConstant.screenHeight/3+220, FlakesConstant.screenWidth*7/20, 40))
        
        loginButton.backgroundColor = UIColor.LightBlue
        loginButton.layer.borderColor = UIColor.whiteColor().CGColor
        loginButton.layer.borderWidth = 1.5
        loginButton.layer.cornerRadius = 10
        loginButton.setTitle("Signup", forState: .Normal)
        loginButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        loginButton.addTarget(self, action: #selector(signup), forControlEvents: .TouchUpInside)
        
        self.view.addSubview(loginButton)
        
        
        let cancelButton = UIButton(frame: CGRectMake(FlakesConstant.screenWidth*2/20, FlakesConstant.screenHeight/3+220, FlakesConstant.screenWidth*7/20, 40))
        
        cancelButton.backgroundColor = UIColor.whiteColor()
        cancelButton.layer.borderColor = UIColor.LightBlue.CGColor
        cancelButton.layer.borderWidth = 1.5
        cancelButton.layer.cornerRadius = 10
        cancelButton.setTitle("Cancel", forState: .Normal)
        cancelButton.setTitleColor(UIColor.LightBlue, forState: .Normal)
        cancelButton.addTarget(self, action: #selector(dismiss), forControlEvents: .TouchUpInside)
        
        self.view.addSubview(cancelButton)
        
        
        errorLabel.frame = CGRectMake(FlakesConstant.screenWidth*3/10, FlakesConstant.screenHeight/3+185, FlakesConstant.screenWidth*4/10, 30)
        
        errorLabel.backgroundColor = UIColor.LightBlue
        errorLabel.font = UIFont(name: "Arial", size: 15)
        errorLabel.textColor = UIColor.whiteColor()
        errorLabel.textAlignment = NSTextAlignment.Center
        errorLabel.alpha = 0
        self.view.addSubview(errorLabel)
        
    }
    
    func showError(error:String) {
        
        dismissKeyboard()
        self.errorLabel.alpha = 0.0
        self.errorLabel.text = error
        
        UIView.animateWithDuration(0.2, delay: 0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
            self.errorLabel.alpha = 1
            
            }, completion: {(Bool) in
        })
        
    }
    
    
    func cleanError() {
        
        self.errorLabel.text = ""
        
    }
    
    
    func signup() {
        
        let userName = userBox.text?.lowercaseString
        let email = emailBox.text?.lowercaseString
        let password = passwordBox.text?.lowercaseString
        let passwordConfirmation = passwordReenterBox.text?.lowercaseString
        
        
        let errorMessage = isSignupDataReady(userName, email: email, password: password, passwordConfirmation: passwordConfirmation)
        
        if (errorMessage == nil) {
            
            cleanError()
            
            let email = String.trimString(email!.lowercaseString)
            dismissKeyboard()
            
            loadingView.showMiddle()
            
            //TODO: need to handle buyer sign up too.
            authManager.signup(email, password: password!, userName: userName!) { (result:Bool) in
                if (result) {
                    self.dismiss()
                    NSNotificationCenter.defaultCenter().postNotificationName("USER_AUTHENTICATED", object: nil)
                } else {
                    self.showError("Sign up failed")
                }
                
                self.loadingView.hide()
            }
            
        } else {
            showError(errorMessage!)
        }
        
    }
    
    func isSignupDataReady(userName:String?,email:String?, password:String?, passwordConfirmation:String?) -> String? {
        if  userName == nil ||
            email == nil ||
            password == nil ||
            passwordConfirmation == nil {
            
            return "Please enter all fields"
        }
        
        let pw: String = String.trimString(password!)
        
        if  String.trimString(userName!).characters.count == 0 || String.trimString(email!).characters.count == 0 || pw.characters.count == 0 || String.trimString(passwordConfirmation!).characters.count == 0 {
            
            return "Please enter all fields"
        }
        
        if password != passwordConfirmation {
            
            return "Passwords don't match"
        }
        
        //TODO
        
        //        if !isValidEmail(email!) {
        //
        //            return VimConstants.InvalidPhoneNumber
        //        }
        
        //        if pw.characters.count < 8 {
        //
        //            return VimConstants.InvalidPasswordLength
        //        }
        
        return nil
    }
    
    
    func dismissKeyboard() {
        
        self.emailBox.resignFirstResponder()
        self.passwordReenterBox.resignFirstResponder()
        self.passwordBox.resignFirstResponder()
        
    }
    
    func dismiss() {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
