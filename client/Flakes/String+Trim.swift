//
//  String+Trim.swift
//  BoxFox
//
//  Created by Zixuan Li on 5/20/15.
//  Copyright (c) 2015 BoxFox. All rights reserved.
//

import UIKit

public extension String {
   
    static func trimString(str: String) -> String {
        return str.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
    }
    
    static func trimForDisplay(string string:String) -> String {
        if (string.characters.count > 8) {
            let index = string.startIndex.advancedBy(7)
            var newstring = (string.substringToIndex(index))
            newstring = newstring + "..."
            return newstring
        } else {
            return string
        }
    }
    
}
