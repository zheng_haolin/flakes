//
//  AuthenticationManager.swift
//  vim-toys
//
//  Created by juan kou on 1/21/16.
//  Copyright © 2016 vim.com. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

public class AuthenticationManager: NSObject {
    
    func login(email:String, password:String, completion:((Bool) -> Void)) {
        
        //get params
        let param = login_params(email, password: password)
        print(param)
        
        HTTPManager.sharedInstance.manager?.request(.POST, FlakesAPI.getURLFor(), parameters: param, encoding: .JSON, headers: nil).responseJSON(completionHandler: { (response) -> Void in
            
            if response.result.isSuccess {
                
                print(response)
                let json = JSON(response.result.value!)
                self.saveLoginCredentials(json, email: email)
                completion(true)
            
            } else {
                
                print(response.result.error)
                completion(false)
            }
        })

    }
    
    func signup(email:String, password:String, userName:String, completion:((Bool) -> Void)) {
        
        //get params
        let param = signup_params(email, password: password, userName: userName)
        print(param)
        HTTPManager.sharedInstance.manager?.request(.POST, FlakesAPI.getURLFor(), parameters: param, encoding: .JSON, headers: nil).responseJSON(completionHandler: { (response) -> Void in
            
            if response.result.isSuccess {
                
                print(response)
                
                let json = JSON(response.result.value!)
                self.saveSignupCredentials(json, email: email)
                completion(true)
                
            } else {
                
                print(response.result.error)
                completion(false)
            }
        })
        
    }
    
    func login_params(email:String, password:String) -> Dictionary<String, String> {
        let params:[String:String] = ["RequestType": RequestType.Signin,
                      "UserGuid": email,
                      "UserPassword": password,
                      "DeviceToken": DefaultManager.sharedInstance.getPushNotificationToken()!
        ]
        return params
    }
    
    func signup_params(email:String, password:String, userName:String) -> Dictionary<String, String> {
        let params:[String:String] = ["RequestType": RequestType.Signup,
                      "UserGuid": email,
                      "UserName": userName,
                      "UserPassword": password,
                      "DeviceToken": DefaultManager.sharedInstance.getPushNotificationToken()!]
        return params
    }
    
    func saveLoginCredentials(json:JSON, email:String) {
        let token = json["Token"].string
        let name = json["UserName"].string
        print(token)
        DefaultManager.sharedInstance.setUserEmail(email)
        DefaultManager.sharedInstance.setToken(token!)
        DefaultManager.sharedInstance.setUserName(name!)
        
        print("Login success with userguid: " + DefaultManager.sharedInstance.getToken()!)
    }
    
    func saveSignupCredentials(json:JSON, email:String) {
        let token = json["Token"].string
        let name = json["UserName"].string
        DefaultManager.sharedInstance.setUserEmail(email)
        DefaultManager.sharedInstance.setToken(token!)
        DefaultManager.sharedInstance.setUserName(name!)
        
        print("Sign up success with userguid: " + DefaultManager.sharedInstance.getToken()!)
        
    }
    
    // cache device token
    func cacheDeviceToken(token:String) {
        DefaultManager.sharedInstance.savePushNotificationToken(token: token)
    }

    
    /// remove push notification on server
    func removeDeviceToken() {
//        let token = DefaultManager.sharedInstance.getPushNotificationToken()
//        if let t = token {
//            let url = VimAPI.getURLFor(VimAPI.DELETE_PUSH_NOTIFICATION_TOKEN) + t
//            VimHTTPManager.sharedInstance.manager?.request(.DELETE, url, parameters: nil, encoding: .URL, headers: nil).responseJSON(completionHandler: { (response) -> Void in
//                if response.result.isSuccess {
//                    print(response.result.value)
//                } else {
//                    print(response.result.error)
//                }
//            })
//        }
    }
    
    public func removeData() {
//        let pushNotificationManager = PushNotificationManager()
//        pushNotificationManager.removeDeviceToken()
//        DefaultManager.sharedInstance.clearUserData()
    }
}