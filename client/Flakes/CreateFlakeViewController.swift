//
//  CreateFlakeViewController.swift
//  Flakes
//
//  Created by Kelvin Deng on 7/23/16.
//  Copyright © 2016 Flakes.com. All rights reserved.
//

import UIKit
import KMPlaceholderTextView
import pop
import SwiftyJSON
import Foundation
import CoreLocation

class CreateFlakeViewController: UIViewController, CLLocationManagerDelegate, UITextViewDelegate, UITextFieldDelegate {
    
    var isPublic:Bool
    var contentBox:KMPlaceholderTextView
    var questionView:UIView
    
    var receiverBox:UITextField
    var questionBox:UITextField
    var answerBox1:UITextField
    var answerBox2:UITextField
    var answerBox3:UITextField
    
    var skipAndSubmitButton:UIButton
    var closeButton:UIButton
    var submitButton:UIButton
    
    var correctAnswerIndex:String
    var shouldSkipQuestion:Bool
    
    
    var longitude:Double = 0
    var latitude:Double = 0
    
    let loadingView = LoadingView()
    let infoView:UIView = UIView()
    let infoImage:UIImageView = UIImageView()
    let infoText:UILabel = UILabel()
    let flakesSyncEngine = FlakesSyncEngine()
    let locationManager:CLLocationManager = CLLocationManager()
    
    init(isPublic:Bool) {
        self.isPublic = isPublic
        self.contentBox = KMPlaceholderTextView(frame: CGRectMake(0, 0, FlakesConstant.screenWidth, FlakesConstant.screenHeight))
        self.questionView = UIView()
        self.questionBox = UITextField()
        self.answerBox1 = UITextField()
        self.answerBox2 = UITextField()
        self.answerBox3 = UITextField()
        self.skipAndSubmitButton = UIButton()
        self.closeButton = UIButton()
        self.receiverBox = UITextField()
        self.skipAndSubmitButton = UIButton()
        self.submitButton = UIButton()
        self.correctAnswerIndex = "1"
        self.shouldSkipQuestion = false
        super.init(nibName: nil, bundle: nil)
        
        self.view.backgroundColor = UIColor.SuperLightBlue
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.sharedApplication().statusBarHidden = true
        
        setBgView()
        setReceiverBox()
        setContentBox()
        setQuestionView()
        setCloseButton()
        setInfo()
        
        setLocationManager()
        
        if (isPublic) {
            contentBox.alpha = 1
            contentBox.becomeFirstResponder()
        } else {
            receiverBox.alpha = 1
            receiverBox.becomeFirstResponder()
        }

    }
    
    func setInfo() {
        infoView.alpha = 0
        infoView.frame = CGRectMake(0, 0, FlakesConstant.screenWidth , FlakesConstant.screenHeight )
        
        infoView.backgroundColor = UIColor.whiteColor()
        
        infoImage.frame = CGRectMake((FlakesConstant.screenWidth - 100) / 2, 150, 100, 100)
        infoText.frame = CGRectMake(50, 150 + 100 + 20, FlakesConstant.screenWidth - 100, 100)
        let okButton = UIButton(frame: CGRectMake(50, FlakesConstant.screenHeight - 40 - 30, FlakesConstant.screenWidth - 100, 40))
        
        okButton.addTarget(self, action: #selector(hideInfo), forControlEvents: .TouchUpInside)
        okButton.backgroundColor = UIColor.LightBlue
        okButton.setTitle("OK", forState: .Normal)
        okButton.layer.cornerRadius = 6
        
        infoText.font = UIFont.FlakesFont(size: 23)
        infoText.numberOfLines = 0
        infoText.lineBreakMode = .ByWordWrapping
        infoText.textAlignment = .Center
        
        infoView.addSubview(infoImage)
        infoView.addSubview(infoText)
        infoView.addSubview(okButton)
        
        self.view.addSubview(infoView)
    }
    
    func setBgView() {
        
        let bgView = UIImageView(frame: CGRectMake(0, 0, FlakesConstant.screenWidth, 30))
        bgView.image = UIImage(named: "Rainbow")
        let visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .Light))
        visualEffectView.frame = bgView.bounds
        //bgView.addSubview(visualEffectView)
        self.view.addSubview(bgView)
    }
    
    func setReceiverBox() {
        
        receiverBox.frame = CGRectMake(0, 150, FlakesConstant.screenWidth, 50)
        receiverBox.textAlignment = NSTextAlignment.Center
        receiverBox.font = UIFont.FlakesFont(size: 25)
        receiverBox.returnKeyType = UIReturnKeyType.Next
        receiverBox.alpha = 0
        receiverBox.placeholder = "Receiver ID"
        receiverBox.delegate = self
        receiverBox.tintColor = UIColor.tintColor
        receiverBox.textColor = UIColor.whiteColor()
        view.addSubview(receiverBox)
    }
    
    func setContentBox() {
        
        contentBox.alpha = 0
        contentBox.placeholder = "Leave a message"
        
        contentBox.frame = CGRectMake(0, 150, FlakesConstant.screenWidth, 150)
    
        contentBox.textAlignment = NSTextAlignment.Center
        contentBox.font = UIFont.FlakesFont(size: 25)
        contentBox.textColor = UIColor.blackColor()
        contentBox.backgroundColor = UIColor.clearColor()
        
        contentBox.textColor = UIColor.whiteColor()
        contentBox.tintColor = UIColor.tintColor
        
        contentBox.delegate = self
        contentBox.returnKeyType = UIReturnKeyType.Done
        view.addSubview(self.contentBox)
    }
    
    func setQuestionView() {
        
        questionView.frame = CGRectMake(0, 50, FlakesConstant.screenWidth, 300)
        
        var top:CGFloat = 40
        let padding:CGFloat = 10
        let questionHeight:CGFloat = 50
        let answerHeight:CGFloat = 20
        let left:CGFloat = 20
        let boxWidth:CGFloat = FlakesConstant.screenWidth - left * 2
        
        questionBox.frame = CGRectMake(left, top, boxWidth, 50)
        top = top + questionHeight + padding
        answerBox1.frame = CGRectMake(left, top, boxWidth, answerHeight)
        top = top + answerHeight + padding
        answerBox2.frame = CGRectMake(left, top, boxWidth, answerHeight)
        top = top + answerHeight + padding
        answerBox3.frame = CGRectMake(left, top, boxWidth, answerHeight)
        top = top + answerHeight + 50
        
        setSubmitButtons(top)
        setAnswerBoxParams(answerBox1)
        setAnswerBoxParams(answerBox2)
        setAnswerBoxParams(answerBox3)
        
        questionBox.placeholder = "What is your question?"
        questionBox.tintColor = UIColor.tintColor
        
        answerBox1.placeholder = "First Answer"
        answerBox2.placeholder = "Second Answer"
        answerBox3.placeholder = "Third Answer"
        
        questionBox.textAlignment = .Center
        questionBox.font = UIFont.FlakesFont(size: 22)
        questionBox.textColor = UIColor.whiteColor()
        
        questionView.addSubview(questionBox)
        questionView.addSubview(answerBox1)
        questionView.addSubview(answerBox2)
        questionView.addSubview(answerBox3)
        
        questionView.alpha = 0
        
        self.view.addSubview(questionView)
        
    }
    
    func setCloseButton() {
        closeButton.frame = CGRectMake(FlakesConstant.screenWidth - 50 - 20, 20, 40, 40)
        closeButton.setImage(UIImage(named: "cancel_white"), forState: .Normal)
        closeButton.imageView?.contentMode = .ScaleToFill
        closeButton.addTarget(self, action: #selector(CreateFlakeViewController.closeVC), forControlEvents: .TouchUpInside)
        self.view.addSubview(closeButton)
    }

    func setSubmitButtons(top:CGFloat) {
        //submit buttons
        self.questionView.addSubview(submitButton)
        self.questionView.addSubview(skipAndSubmitButton)
        
        let padding:CGFloat = 18
        let width = (FlakesConstant.screenWidth - padding * 3 ) / 2
        let height:CGFloat = 40
        submitButton.frame = CGRectMake(padding, top, width , height)
        skipAndSubmitButton.frame = CGRectMake(padding * 2 + width, top, width , height)
        
        submitButton.backgroundColor = UIColor.LightBlue
        skipAndSubmitButton.backgroundColor = UIColor.LightBlue
        
        submitButton.setTitle("Submit", forState: .Normal)
        skipAndSubmitButton.setTitle("Skip and Submit", forState: .Normal)
        
        submitButton.addTarget(self, action: #selector(CreateFlakeViewController.submitWithQuestion), forControlEvents: .TouchUpInside)
        skipAndSubmitButton.addTarget(self, action: #selector(CreateFlakeViewController.submitWithoutQuestion), forControlEvents: .TouchUpInside)
    }
    
    func setLocationManager() {
        
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    //MARK action
    func closeVC() {
        self.dismissViewControllerAnimated(true, completion: nil)
        self.loadingView.hide()
    }
    
    func submitWithoutQuestion() {
        shouldSkipQuestion = true
        submit()
    }
    
    func submitWithQuestion() {
        
        if (!shouldSkipQuestion && questionBox.text != "") {
            showPickAnswerAction()
        } else {
            self.questionBox.placeholder = "Please enter a question:)"
        }
    }
    
    func submit() {
        
        //loadingView.showMiddle()
        
        let param = setSubmitFlakeParam()
        
        flakesSyncEngine.createFlake(param) {
            (result:Bool) in
            self.hideKeyboard()
            if (result) {
                //self.loadingView.hide()
                self.showInfo(UIImage(named: "lovely")!, text: "Your snowFlake is dropped! Wait for someone to pick it up")
            } else {
                //self.loadingView.hide()
                self.showInfo(UIImage(named: "embarrassed-1")!, text: "Sorry, snowFlake is not created, please try later")
            }
        }
        
    }
    
    func showQuestion() {
        
        UIView.animateWithDuration(1, animations:  {() in
            
            self.questionView.alpha = 1
            
        }, completion:{(Bool)  in
            self.questionBox.becomeFirstResponder()
        })
    }
    
    func setAnswerBoxParams(textField:UITextField) {
        textField.font = UIFont.FlakesFont(size: 18)
        textField.delegate = self
        textField.returnKeyType = UIReturnKeyType.Next
        textField.textAlignment = .Center
        textField.tintColor = UIColor.tintColor
        textField.textColor = UIColor.whiteColor()
    }
    
    func showPickAnswerAction() {
        let optionMenu = UIAlertController(title: nil, message: "Set Correct Answer", preferredStyle: .ActionSheet)
        
        let answer1Action = UIAlertAction(title: answerBox1.text!, style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.correctAnswerIndex = "1"
            self.submit()
        })
        
        let answer2Action = UIAlertAction(title: answerBox2.text!, style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.correctAnswerIndex = "2"
            self.submit()
        })
        
        let answer3Action = UIAlertAction(title: answerBox3.text!, style: .Default, handler: {
            (alert: UIAlertAction!) -> Void in
            self.correctAnswerIndex = "3"
            self.submit()
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel, handler: {
            (alert: UIAlertAction!) -> Void in
        })
        
        
        // 4
        optionMenu.addAction(answer1Action)
        optionMenu.addAction(answer2Action)
        optionMenu.addAction(answer3Action)
        optionMenu.addAction(cancelAction)
        
        // 5
        self.presentViewController(optionMenu, animated: true, completion: nil)
    }
    
    //MARK TextView delegate
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
            contentBox.resignFirstResponder()
            contentBox.alpha = 0
            showQuestion()
        }
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        switch textField {
        
        case questionBox:
            questionBox.resignFirstResponder()
            answerBox1.becomeFirstResponder()
        case receiverBox:
            receiverBox.resignFirstResponder()
            receiverBox.alpha = 0
            
            UIView.animateWithDuration(1, animations:  {() in
                self.contentBox.alpha = 1
            }, completion:{(Bool)  in
                self.contentBox.becomeFirstResponder()
            })

        case answerBox1:
            
            answerBox1.resignFirstResponder()
            answerBox2.becomeFirstResponder()
        case answerBox2:
            
            answerBox2.resignFirstResponder()
            answerBox3.becomeFirstResponder()
        case answerBox3:

            answerBox3.resignFirstResponder()
            
        default:
            break
        }
        
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK location manager delegate
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue:CLLocationCoordinate2D = manager.location!.coordinate
        
        longitude = locValue.longitude
        latitude = locValue.latitude
    }

    //MARK JSON PARAM
    func setSubmitFlakeParam() -> AnyObject {
        
        var param:[String:AnyObject] = ["RequestType": RequestType.CreateFlake,
                                        "DeviceToken": DefaultManager.sharedInstance.getPushNotificationToken()!,
                                        "Token": DefaultManager.sharedInstance.getToken()!
                                        ]
                                        
        
        let question:[String:AnyObject] = ["Question":[ "QuestionText": questionBox.text!,
                                                        "Answer1": answerBox1.text!,
                                                        "Answer2": answerBox2.text!,
                                                        "Answer3": answerBox3.text!,
                            
                                           ]]
        
        var flake:[String:AnyObject] =  [ "Content": contentBox.text,
                                          "Longitude": longitude,
                                          "Latitude": latitude,
                                          "UserName": DefaultManager.sharedInstance.getUserName()!,
                                          "QuestionCorrectAnswer": correctAnswerIndex
                                        ]
        
        if (isPublic) {
            param["Flake"] = flake
            if questionBox.text != "" && !shouldSkipQuestion {
                //has question
                param["Question"] = question
            }
        } else {
            //private
            //add receiver
            flake["ReceiverGuid"] = receiverBox.text!
            if questionBox.text != "" && !shouldSkipQuestion {
                //has question
                param["Question"] = question
            }
            param["Flake"] = flake
        }
        
        return param
    }
    
    func hideKeyboard() {
        answerBox1.resignFirstResponder()
        answerBox2.resignFirstResponder()
        answerBox3.resignFirstResponder()
        questionBox.resignFirstResponder()
    }

    
    func showInfo(img:UIImage, text:String) {
        infoImage.image = img
        infoText.text = text
        
        UIView.animateWithDuration(1, animations: {
            self.infoView.alpha = 1
            }, completion: {
                (value: Bool) in
                
        })
        
        self.infoView.alpha = 1
        
    }
    
    func hideInfo() {
        
//        UIView.animateWithDuration(1, animations: {
//            self.infoView.alpha = 0
//        }, completion: {
//                (value: Bool) in
//            self.dismissViewControllerAnimated(true, completion: nil)
//        })
        self.dismissViewControllerAnimated(true, completion: nil)
    }


}
