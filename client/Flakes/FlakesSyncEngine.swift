//
//  FlakesSyncEngine.swift
//  Flakes
//
//  Created by juan kou on 7/23/16.
//  Copyright © 2016 Flakes.com. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class FlakesSyncEngine: NSObject {
    
    func getInactiveFlakes(completion:((Bool, JSON?) -> Void)) {
        
        let param:[String:AnyObject] = ["RequestType":RequestType.GetFlakeAsOwner, "Token":DefaultManager.sharedInstance.getToken()!]
        print(param)
        
        HTTPManager.sharedInstance.manager?.request(.POST, FlakesAPI.getURLFor(), parameters: param, encoding: .JSON, headers: nil).responseJSON(completionHandler: { (response) -> Void in
            
            if response.result.isSuccess {
                
                print(response.result.value)
                completion(true, JSON(response.result.value!))
            } else {
                
                print(response.result.error)
                completion(false, nil)
            }
        })
    }
    
    func getActiveFlakes(completion:((Bool, JSON?) -> Void)) {
        
        let param:[String:AnyObject] = ["RequestType":RequestType.GetFlakeChat, "Token":DefaultManager.sharedInstance.getToken()!]
        print(param)
        
        HTTPManager.sharedInstance.manager?.request(.POST, FlakesAPI.getURLFor(), parameters: param, encoding: .JSON, headers: nil).responseJSON(completionHandler: { (response) -> Void in
            
            if response.result.isSuccess {
                
                print(response.result.value)
                completion(true, JSON(response.result.value!))
            } else {
                
                print(response.result.error)
                completion(false, nil)
            }
        })
    }
    
    func createFlake(param:AnyObject, completion:((Bool) -> Void)) {
        
        print(param)
        
        HTTPManager.sharedInstance.manager?.request(.POST, FlakesAPI.getURLFor(), parameters: param as? [String : AnyObject], encoding: .JSON, headers: nil).responseJSON(completionHandler: { (response) -> Void in
            
            if response.result.isSuccess {
                
                print(response)
                completion(true)
            } else {
                
                print(response.result.error)
                completion(false)
            }
        })

    }
    
    func getFlakeBuckets(param:AnyObject, completion:((Bool, JSON?) -> ())) {
        
        print("getting buckets")
        HTTPManager.sharedInstance.manager?.request(.POST, FlakesAPI.getURLFor(), parameters: param as? [String : AnyObject], encoding: .JSON, headers: nil).responseJSON(completionHandler: { (response) -> Void in
            
            if response.result.isSuccess {
                
                print(response)
                let json = JSON(response.result.value!)
                completion(true, json)
            } else {
                
                print(response.result.error)
                completion(false, nil)
            }
        })
        
    }
    
    func getQuestion(param:AnyObject, completion:((Bool, JSON?) -> ())) {
        
        print("getting question")
        HTTPManager.sharedInstance.manager?.request(.POST, FlakesAPI.getURLFor(), parameters: param as? [String : AnyObject], encoding: .JSON, headers: nil).responseJSON(completionHandler: { (response) -> Void in
            
            if response.result.isSuccess {
                
                print(response)
                let json = JSON(response.result.value!)
                completion(true, json)
            } else {
                
                print(response.result.error)
                completion(false, nil)
            }
        })
        
    }

    func getContent(param:AnyObject, completion:((Bool, JSON?) -> ())) {
        
        print("getting content")
        HTTPManager.sharedInstance.manager?.request(.POST, FlakesAPI.getURLFor(), parameters: param as? [String : AnyObject], encoding: .JSON, headers: nil).responseJSON(completionHandler: { (response) -> Void in
            
            if response.result.isSuccess {
                
                print(response)
                let json = JSON(response.result.value!)
                print(param)
                completion(true, json)
            } else {
                
                print(response.result.error)
                completion(false, nil)
            }
        })
    }


}
