//
//  ProfileViewController.swift
//  Flakes
//
//  Created by DuRuitao on 7/25/16.
//  Copyright © 2016 Flakes.com. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {

    // Profile
    var upperLabel = UILabel()
    var lowerLabel = UILabel()
    var usernameLabel = UILabel()
    var useremailLabel = UILabel()
    var totalFlakeLabel = UILabel()
    var totalFlakeOpenedLabel = UILabel()
    var activeFlakeLabel = UILabel()
    
    var totalFlakeLabelUser = UILabel()
    var totalFlakeOpenedLabelUser = UILabel()
    var activeFlakeLabelUser = UILabel()
    
    let myImgView:UIImageView = UIImageView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUpperFrame()
        setupLowerFrame()
        setupAvater()
        setupUsernameLabel()
        setupUseremailLabel()
        getUserData()
        setButton()
        
        setupActiveFlakesLabel()
        setupTotalFlakesLabel()
        setuptotalFlakeOpenedLabel()
        
        setupActiveFlakesUserLabel()
        setupTotalFlakesUserLabel()
        setuptotalFlakeOpenedUserLabel()

    }
    
    func logOut() {
        DefaultManager.sharedInstance.clearUserData()
        let appDelegate = UIApplication.sharedApplication().delegate! as! AppDelegate
        var initialViewController = self.storyboard!.instantiateViewControllerWithIdentifier("MainPage")
        appDelegate.window?.rootViewController = initialViewController
        appDelegate.window?.makeKeyAndVisible()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setupUpperFrame(){

        upperLabel.frame = CGRectMake(0, 0, FlakesConstant.screenWidth, FlakesConstant.screenHeight*1/3)
        upperLabel.backgroundColor = UIColor.whiteColor()
        
        self.view.addSubview(upperLabel)
    }
    
    func setupLowerFrame(){
        
        lowerLabel.frame = CGRectMake(0, FlakesConstant.screenHeight*1/3, FlakesConstant.screenWidth,FlakesConstant.screenHeight*2/3)
        lowerLabel.backgroundColor = UIColor.LightBlue
        
        self.view.addSubview(lowerLabel)
    }
    
    func setupAvater(){
        myImgView.frame = CGRectMake(FlakesConstant.screenWidth*1/10, FlakesConstant.screenHeight*1/3-80-FlakesConstant.screenWidth*1/10, 80, 80)
        self.view.addSubview(myImgView)
    }
    
    func setupUsernameLabel(){
        usernameLabel.frame = CGRectMake(FlakesConstant.screenWidth*4/10, FlakesConstant.screenHeight*1/3-80-FlakesConstant.screenWidth*1/10, FlakesConstant.screenWidth*4/5,40)
        usernameLabel.textColor = UIColor.blackColor()
        usernameLabel.textAlignment = .Left
        usernameLabel.font = UIFont(name : "HelveticaNeue-Bold", size:20)
        self.view.addSubview(usernameLabel)
    }
    
    func setupUseremailLabel(){
        useremailLabel.frame = CGRectMake(FlakesConstant.screenWidth*4/10, FlakesConstant.screenHeight*1/3-40-FlakesConstant.screenWidth*1/10, FlakesConstant.screenWidth*4/5,40)
        useremailLabel.textColor = UIColor.blackColor()
        useremailLabel.textAlignment = .Left
        self.view.addSubview(useremailLabel)
    }
    
    func getUserData(){
        // get data from user
        
        usernameLabel.text = DefaultManager.sharedInstance.getUserName()
        useremailLabel.text =  DefaultManager.sharedInstance.getUserEmail()
        totalFlakeLabelUser.text = "69"
        totalFlakeOpenedLabelUser.text = "1231"
        activeFlakeLabelUser.text = "5"
        myImgView.setImageWithString(usernameLabel.text, color: UIColor.LightBlue, circular: true)
    }
    
    func setupTotalFlakesLabel(){
        totalFlakeLabel.frame = CGRectMake(FlakesConstant.screenWidth*1/10, FlakesConstant.screenHeight*4/10, FlakesConstant.screenWidth*2/5,40)
        totalFlakeLabel.backgroundColor = UIColor.LightBlue
        totalFlakeLabel.textColor = UIColor.whiteColor()
        totalFlakeLabel.textAlignment = .Left
        totalFlakeLabel.font = UIFont(name : "HelveticaNeue", size:13)
        totalFlakeLabel.text = "Total Flakes Dropped:"
        self.view.addSubview(totalFlakeLabel)
    }
    
    func setuptotalFlakeOpenedLabel(){
        totalFlakeOpenedLabel.frame = CGRectMake(FlakesConstant.screenWidth*1/10, FlakesConstant.screenHeight*4/10+45, FlakesConstant.screenWidth*2/5,40)
        totalFlakeOpenedLabel.backgroundColor = UIColor.LightBlue
        totalFlakeOpenedLabel.textColor = UIColor.whiteColor()
        totalFlakeOpenedLabel.textAlignment = .Left
        totalFlakeOpenedLabel.font = UIFont(name : "HelveticaNeue", size:13)
        totalFlakeOpenedLabel.text = "Total Flakes Opened:"
        self.view.addSubview(totalFlakeOpenedLabel)
    }
    
    func setupActiveFlakesLabel(){
        activeFlakeLabel.frame = CGRectMake(FlakesConstant.screenWidth*1/10, FlakesConstant.screenHeight*4/10+90, FlakesConstant.screenWidth*2/5,40)
        activeFlakeLabel.backgroundColor = UIColor.LightBlue
        activeFlakeLabel.textColor = UIColor.whiteColor()
        activeFlakeLabel.textAlignment = .Left
        activeFlakeLabel.font = UIFont(name : "HelveticaNeue", size:13)
        activeFlakeLabel.text = "Active Flakes:"
        self.view.addSubview(activeFlakeLabel)
    }
    
    func setupTotalFlakesUserLabel(){
        totalFlakeLabelUser.frame = CGRectMake(FlakesConstant.screenWidth*3/5, FlakesConstant.screenHeight*4/10, FlakesConstant.screenWidth*2/5,40)
        totalFlakeLabelUser.backgroundColor = UIColor.LightBlue
        totalFlakeLabelUser.textColor = UIColor.whiteColor()
        totalFlakeLabelUser.textAlignment = .Left
        self.view.addSubview(totalFlakeLabelUser)
    }
    
    func setuptotalFlakeOpenedUserLabel(){
        totalFlakeOpenedLabelUser.frame = CGRectMake(FlakesConstant.screenWidth*3/5, FlakesConstant.screenHeight*4/10+45, FlakesConstant.screenWidth*2/5,40)
        totalFlakeOpenedLabelUser.backgroundColor = UIColor.LightBlue
        totalFlakeOpenedLabelUser.textColor = UIColor.whiteColor()
        totalFlakeOpenedLabelUser.textAlignment = .Left
        self.view.addSubview(totalFlakeOpenedLabelUser)
    }
    
    func setupActiveFlakesUserLabel(){
        activeFlakeLabelUser.frame = CGRectMake(FlakesConstant.screenWidth*3/5, FlakesConstant.screenHeight*4/10+90, FlakesConstant.screenWidth*2/5,40)
        activeFlakeLabelUser.backgroundColor = UIColor.LightBlue
        activeFlakeLabelUser.textColor = UIColor.whiteColor()
        activeFlakeLabelUser.textAlignment = .Left
        self.view.addSubview(activeFlakeLabelUser)
    }
    
    func setButton(){
        let logout = UIButton(frame: CGRectMake(FlakesConstant.screenWidth*3/10,FlakesConstant.screenHeight*8/10,FlakesConstant.screenWidth*2/5,40))
        logout.backgroundColor = UIColor.whiteColor()
        logout.layer.cornerRadius = 10
        logout.setTitle("Sign out", forState: .Normal)
        logout.setTitleColor(UIColor.LightBlue, forState: .Normal)
        
        logout.addTarget(self, action: #selector(logOut), forControlEvents: .TouchUpInside)
        self.view.addSubview(logout)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
