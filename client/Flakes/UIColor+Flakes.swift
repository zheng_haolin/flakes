import UIKit

extension UIColor {
    
    static var White:UIColor {
        get{
            return UIColor(red:0.98, green:0.97, blue:0.61, alpha:1.0)
        }
    }
    
    static var HyperLightBlue:UIColor {
        get{
            return UIColor(red:0.000, green:0.314, blue:0.690, alpha:0.7)
        }
    }
    
    static var SuperLightBlue:UIColor {
        get{
            return UIColor(red:0.565, green:0.890, blue:0.949, alpha:1.0)
        }
    }
    
    static var LightBlue:UIColor {
        get {
            return UIColor(red:0.000, green:0.765, blue:0.882, alpha:1.0)
        }
    }
    
    static var DarkBlue:UIColor {
        get {
            return UIColor(red:0.000, green:0.314, blue:0.690, alpha:1.0)
        }
    }

    static var Green:UIColor {
        get {
            return UIColor(red:0.000, green:0.635, blue:0.369, alpha:1.0)
        }
    }

    static var LightYellow:UIColor {
        get {
            return UIColor(red:0.988, green:0.831, blue:0.416, alpha:1.0)
        }
    }

    static var DarkYellow:UIColor {
        get {
            return UIColor(red:0.980, green:0.694, blue:0.133, alpha:1.0)
        }
    }
    
    static var Orange:UIColor {
        get {
            return UIColor(red:0.984, green:0.322, blue:0.173, alpha:1.0)
        }
    }
    
    static var LightGray:UIColor {
        get {
            return UIColor(red:0.84, green:0.85, blue:0.84, alpha:0.3)
        }
    }

    static var DarkPink:UIColor {
        get {
            return UIColor(red:0.992, green:0.380, blue:0.424, alpha:1.0)
        }
    }
    
    static var LightPink:UIColor {
        get {
            return UIColor(red:0.992, green:0.765, blue:0.882, alpha:1.0)
        }
    }
    
    static var tintColor:UIColor {
        get {
            return UIColor(red: 0.439, green: 0.694, blue: 0.761, alpha: 1)
        }
    }
    
    static var TableViewBgColor:UIColor {
        get {
            return UIColor(red: 0.957, green: 0.957, blue: 0.957, alpha: 1)
        }
    }
    
}


