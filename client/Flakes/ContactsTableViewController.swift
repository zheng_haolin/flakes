//
//  ContactsTableViewController.swift
//  Flakes
//
//  Created by DuRuitao on 7/25/16.
//  Copyright © 2016 Flakes.com. All rights reserved.
//

import UIKit

class ContactsTableViewController: UITableViewController {
    
    var contacts:[User] = [User]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.registerClass(ContactsTableViewCell.classForCoder(), forCellReuseIdentifier: "Cell")
        
        contacts = getContacts()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return contacts.count
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 60
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! ContactsTableViewCell

        let contact = self.contacts[indexPath.row]
        // Configure the cell...
        cell.userGuid.text = contact.userGuid
        cell.userName.text = contact.userName
        cell.myImgView.setImageWithString(contact.userName, color: nil, circular: true)
        
        return cell
    }
 
    func getContacts() -> [User]{
        let user0 = User(userGuid: "tobytoyuito@gmail.com", userName: "tobytoyuito")
        let user1 = User(userGuid: "kelvin@outlook.com", userName: "kelvin")
        let user2 = User(userGuid: "harley@outlook.com", userName: "harley")
        let user3 = User(userGuid: "jane@gmail.com", userName: "jane")
        let user4 = User(userGuid: "toby@gmail.com", userName: "toby1")
        let user5 = User(userGuid: "kelvin1@outlook.com", userName: "kelvin1")
        let user6 = User(userGuid: "harley1@outlook.com", userName: "harley1")
        let user7 = User(userGuid: "jane1@gmail.com", userName: "jane1")
        return [user0, user1, user2, user3, user4, user5, user6, user7]
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
