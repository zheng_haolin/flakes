//
//  FlakesAPI.swift
//  Flakes
//
//  Created by juan kou on 7/23/16.
//  Copyright © 2016 Flakes.com. All rights reserved.
//

import Foundation

struct FlakesAPI {
    
    //MARK: API
    static let GET_INACTIVE_FLAKES = "/inactive"
    
    //MARK: helpers
    
    static func getURLFor() -> String {
        return "http://10.104.130.17:8080"

    }
}
