//
//  FlakesConstant.swift
//  Flakes
//
//  Created by Kelvin Deng on 7/23/16.
//  Copyright © 2016 Flakes.com. All rights reserved.
//

import UIKit

class FlakesConstant: NSObject {
    static let screenWidth = UIScreen.mainScreen().bounds.size.width
    static let screenHeight = UIScreen.mainScreen().bounds.size.height
}
