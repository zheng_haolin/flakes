//
//  InfoView.swift
//  Flakes
//
//  Created by juan kou on 7/26/16.
//  Copyright © 2016 Flakes.com. All rights reserved.
//

class InfoView: NSObject {
    
    var bgView:UIView = UIView()

    func show(vc:UIViewController, img:UIImage, text:String) {
        self.bgView.alpha = 1
        bgView.frame = CGRectMake(0, 0, FlakesConstant.screenWidth , FlakesConstant.screenHeight )
        
        bgView.backgroundColor = UIColor.whiteColor()
        
        let imgView = UIImageView(frame: CGRectMake((FlakesConstant.screenWidth - 100) / 2, 150, 100, 100))
        let label = UILabel(frame: CGRectMake(50, 150 + 100 + 20, FlakesConstant.screenWidth - 100, 100))
        let okButton = UIButton(frame: CGRectMake(50, FlakesConstant.screenHeight - 40 - 30, FlakesConstant.screenWidth - 100, 40))
        
        imgView.image = img
        label.text = text
        
        okButton.addTarget(self, action: #selector(InfoView.hide), forControlEvents: .TouchUpInside)
        okButton.backgroundColor = UIColor.LightBlue
        okButton.setTitle("OK", forState: .Normal)
        okButton.layer.cornerRadius = 6
        
        label.font = UIFont.FlakesFont(size: 23)
        label.numberOfLines = 0
        label.lineBreakMode = .ByWordWrapping
        label.textAlignment = .Center
        
        bgView.addSubview(imgView)
        bgView.addSubview(label)
        bgView.addSubview(okButton)
        
        UIApplication.sharedApplication().keyWindow!.addSubview(bgView)
    }
    
    func hide() {
        
        UIView.animateWithDuration(1, animations: {
            self.bgView.alpha = 0
            }, completion: {
                (value: Bool) in
                self.bgView.removeFromSuperview()
        })
        
    }
}

