

import UIKit

public class SignupTableViewController: UITableViewController, UITextFieldDelegate {
    
    var emailCell: InputTableViewCell?
    var passwordCell: InputTableViewCell?
    var passwordConfirmationCell: InputTableViewCell?
    var userNameCell: InputTableViewCell?
    lazy var authManager:AuthenticationManager = AuthenticationManager()
    let loadingView = LoadingView()
    
    lazy var errorLabel: UILabel = {
        
        let labelLeftPadding:CGFloat = 20.0
        let labelY:CGFloat = 3 * 40 + 20
        let labelHeight:CGFloat = 100.0
        var label:UILabel = UILabel(frame: CGRectMake(labelLeftPadding, labelY, FlakesConstant.screenWidth - 2 * labelLeftPadding, labelHeight))
        label.textAlignment = .Center
        label.textColor = UIColor.LightBlue
        label.font = UIFont(name: "Avenir-Medium", size: 18)
        label.backgroundColor = UIColor.clearColor()
        label.lineBreakMode = .ByWordWrapping
        label.numberOfLines = 0
        return label
        
    }()
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.tableView.registerClass(InputTableViewCell.classForCoder(), forCellReuseIdentifier: "Cell")
        self.navigationItem.title = "注册"
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.tableView.backgroundColor = UIColor.whiteColor()
        self.tableView.tableFooterView = UIView(frame: CGRectZero)
        self.view.addSubview(self.errorLabel)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "完成", style: .Plain, target: self, action: #selector(SignupTableViewController.signup))
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .Plain, target: self, action: #selector(dismiss))

    }
    
    
    override public func viewDidAppear(animated: Bool) {
        
        super.viewDidAppear(animated)
        self.userNameCell?.inputField.becomeFirstResponder()
        
    }
    
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override public func viewWillDisappear(animated: Bool) {
        
        super.viewWillDisappear(animated)
        if (self.isMovingFromParentViewController()) {
            
            self.navigationController?.setNavigationBarHidden(true, animated: true)
            dismissKeyboard()
            
        }
        
    }
    
    // MARK: - Table view data source
    
    override public func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
        
    }
    
    
    override public func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 4
        
    }
    
    
    override public func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! InputTableViewCell
        
        switch indexPath.row {
            
        case 0:
            
            cell.configureForPasswordInput()
            cell.inputField.secureTextEntry = false
            cell.inputField.placeholder = "Name"
            cell.inputField.delegate = self
            cell.inputField.tag = 0
            self.userNameCell = cell
            return cell
            
        case 1:
            
            cell.configureForEmailInput()
            cell.inputField.delegate = self
            cell.inputField.tag = 1
            self.emailCell = cell
            return cell
            
        case 2:
            cell.inputField.delegate = self
            cell.inputField.tag = 2
            cell.configureForPasswordInput()
            cell.inputField.returnKeyType = UIReturnKeyType.Next
            self.passwordCell = cell
            return cell
            
        case 3:
            cell.inputField.delegate = self
            cell.inputField.tag = 3
            cell.configureForPasswordConfirmationInput()
            self.passwordConfirmationCell = cell
            return cell
            
        default:
            return cell
            
        }
        
    }
    
    //MARK: UITextFieldDelegate
    public func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        switch textField.tag {
            
        case 0:
            
            self.userNameCell?.inputField.resignFirstResponder()
            self.emailCell?.inputField.becomeFirstResponder()
            
        case 1:
            
            self.emailCell?.inputField.resignFirstResponder()
            self.passwordCell?.inputField.becomeFirstResponder()
            
        case 2:
            
            self.passwordCell?.inputField.resignFirstResponder()
            self.passwordConfirmationCell?.inputField.becomeFirstResponder()
            
        case 3:
            
            signup()
            
        default:
            return true
            
        }
        
        return true
    }
    
    func showError(error:String) {
        
        dismissKeyboard()
        self.errorLabel.alpha = 0.0
        self.errorLabel.text = error
        
        UIView.animateWithDuration(0.2, delay: 0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
            self.errorLabel.alpha = 1
            
            }, completion: {(Bool) in
        })
        
    }
    
    
    func cleanError() {
        
        self.errorLabel.text = ""
        
    }
    
    
    func signup() {
        
        let userName = userNameCell?.inputField.text
        let email = emailCell?.inputField.text
        let password = passwordCell?.inputField.text
        let passwordConfirmation = passwordConfirmationCell?.inputField.text
        
    
        let errorMessage = isSignupDataReady(userName, email: email, password: password, passwordConfirmation: passwordConfirmation)
        
        if (errorMessage == nil) {
            
            cleanError()
            
            let email = String.trimString(email!.lowercaseString)
            dismissKeyboard()
            
            loadingView.showMiddle()
            
            //TODO: need to handle buyer sign up too.
            authManager.signup(email, password: password!, userName: userName!) { (result:Bool) in
                if (result) {
                    self.dismiss()
                    NSNotificationCenter.defaultCenter().postNotificationName("USER_AUTHENTICATED", object: nil)
                } else {
                    self.showError("Sign up failed")
                }
                
                self.loadingView.hide()
            }
            
        } else {
            showError(errorMessage!)
        }
        
    }
    
    func isSignupDataReady(userName:String?,email:String?, password:String?, passwordConfirmation:String?) -> String? {
        if  userName == nil ||
            email == nil ||
            password == nil ||
            passwordConfirmation == nil {
            
            return "Please enter all fields"
        }
        
        let pw: String = String.trimString(password!)
        
        if  String.trimString(userName!).characters.count == 0 || String.trimString(email!).characters.count == 0 || pw.characters.count == 0 || String.trimString(passwordConfirmation!).characters.count == 0 {
            
            return "Please enter all fields"
        }
        
        if password != passwordConfirmation {
            
            return "Passwords don't match"
        }
        
        //TODO
        
//        if !isValidEmail(email!) {
//            
//            return VimConstants.InvalidPhoneNumber
//        }
        
//        if pw.characters.count < 8 {
//            
//            return VimConstants.InvalidPasswordLength
//        }
        
        return nil
    }
    
    
    func dismissKeyboard() {
        
        self.emailCell?.inputField.resignFirstResponder()
        self.passwordCell?.inputField.resignFirstResponder()
        self.passwordConfirmationCell?.inputField.resignFirstResponder()
        
    }
    
    func dismiss() {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func didSignup() {
        
        dismissKeyboard()
        showError("成功注册！")
        
    }
    
    func didFailSignupPhoneNumberIsTaken() {
        
        //showError(VimConstants.RegisteredPhoneNumber)
        
    }
    
    func cannotConnectToServer() {
        
        //showError(VimConstants.CannotConnectToServer)
        
    }
    
    
    deinit {
        
        NSNotificationCenter.defaultCenter().removeObserver(self)
        
    }
    
}

