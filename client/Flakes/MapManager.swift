//
//  MapManager.swift
//  Flakes
//
//  Created by Kelvin Deng on 7/23/16.
//  Copyright © 2016 Flakes.com. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class MapManager: NSObject {
    class func getGoogleMapImageByLocation(longitude:Double, latitude:Double, width:Int, height:Int) -> NSURL {
        
        let imageURL:String = NSString(format:"http://maps.google.com/maps/api/staticmap?markers=icon:http://raw.githubusercontent.com/tobytoyuito/testing/master/snowflake0.png|%f,%f&size=%ix%i&sensor=true&zoom=17",latitude, longitude, width, height) as String
        
        let url = NSURL(string: imageURL.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!)
        return url!
    }
    
    class func getGoogleMapImageByLocation(longitude:Double, latitude:Double) -> NSURL {
        
        let imageURL:String = NSString(format:"http://maps.google.com/maps/api/staticmap?markers=icon:http://raw.githubusercontent.com/tobytoyuito/testing/master/snowflake0.png|%f,%f&size=640x500&sensor=true&zoom=17",latitude, longitude) as String
        
        let url = NSURL(string: imageURL.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!)
        return url!
    }
    
    class func getGoogleReverseGeocoding(longitude:Double, latitude:Double) -> String {
        let url = NSString(format:"https://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f", latitude, longitude) as String
        let url2 = NSURL(string: url.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!)
        let data = NSData(contentsOfURL: url2!)
        var json:AnyObject?
        
        do {
            json = try NSJSONSerialization.JSONObjectWithData(data!, options:[])
            print("Array: \(json)")
        }
        catch {
            print("Error: \(error)")
        }
        
        let new_json = JSON(json!)
        let result = new_json["results"]
        let result0 = result[0]
        let address = result0["formatted_address"].string!
        
        return address
        //let resultJson = JSON(json)["results"]
        //let result0 = resultJson.
        //return result0["formatted_address"].string!
    }
}

