//
//  SignInViewController.swift
//  Flakes
//
//  Created by Xing Gong on 7/25/16.
//  Copyright © 2016 Flakes.com. All rights reserved.
//

import UIKit

class SignInViewController: UIViewController {
    
    var emailBox: UITextField = UITextField();
    var passwordBox: UITextField = UITextField();
    lazy var authManager:AuthenticationManager = AuthenticationManager()
    let loadingView:LoadingView = LoadingView()
    var errorLabel:UILabel = UILabel()

    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        // Do any additional setup after loading the view.
    }
    
    func setUI() {
        self.view.backgroundColor = UIColor.LightBlue
        
        emailBox.frame = CGRectMake(FlakesConstant.screenWidth/10, FlakesConstant.screenHeight/3+10, FlakesConstant.screenWidth*8/10, 40)

        emailBox.placeholder = "Enter your email here"
        emailBox.font = UIFont.systemFontOfSize(15)
        emailBox.borderStyle = UITextBorderStyle.RoundedRect

        emailBox.returnKeyType = UIReturnKeyType.Done
        emailBox.clearButtonMode = UITextFieldViewMode.WhileEditing;
        emailBox.autocapitalizationType = .None
        emailBox.contentVerticalAlignment = UIControlContentVerticalAlignment.Center

        self.view.addSubview(emailBox)

        
        passwordBox.frame = CGRectMake(FlakesConstant.screenWidth/10, FlakesConstant.screenHeight/3+55, FlakesConstant.screenWidth*8/10, 40)
        
        passwordBox.placeholder = "Enter your password here"
        passwordBox.font = UIFont.systemFontOfSize(15)
        passwordBox.borderStyle = UITextBorderStyle.RoundedRect
        passwordBox.secureTextEntry = true

        passwordBox.returnKeyType = UIReturnKeyType.Done
        passwordBox.clearButtonMode = UITextFieldViewMode.WhileEditing;
        passwordBox.contentVerticalAlignment = UIControlContentVerticalAlignment.Center
        
        self.view.addSubview(passwordBox)
        
        
        let loginButton = UIButton(frame: CGRectMake(FlakesConstant.screenWidth*11/20, FlakesConstant.screenHeight/3+145, FlakesConstant.screenWidth*7/20, 40))
        loginButton.addTarget(self, action: #selector(login), forControlEvents: .TouchUpInside)
        
        loginButton.backgroundColor = UIColor.LightBlue
        loginButton.layer.borderColor = UIColor.whiteColor().CGColor
        loginButton.layer.borderWidth = 1.5
        loginButton.layer.cornerRadius = 10
        loginButton.setTitle("Login", forState: .Normal)
        loginButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        loginButton.addTarget(self, action: #selector(login), forControlEvents: .TouchUpInside)
        
        self.view.addSubview(loginButton)
        
        
        let cancelButton = UIButton(frame: CGRectMake(FlakesConstant.screenWidth*2/20, FlakesConstant.screenHeight/3+145, FlakesConstant.screenWidth*7/20, 40))
        
        cancelButton.backgroundColor = UIColor.whiteColor()
        cancelButton.layer.borderColor = UIColor.LightBlue.CGColor
        cancelButton.layer.borderWidth = 1.5
        cancelButton.layer.cornerRadius = 10
        cancelButton.setTitle("Cancel", forState: .Normal)
        cancelButton.setTitleColor(UIColor.LightBlue, forState: .Normal)
        cancelButton.addTarget(self, action: #selector(dismiss), forControlEvents: .TouchUpInside)
        
        self.view.addSubview(cancelButton)
        
        
        errorLabel.frame = CGRectMake(FlakesConstant.screenWidth*3/10, FlakesConstant.screenHeight/3+100, FlakesConstant.screenWidth*4/10, 30)
        
        errorLabel.text = ""
        errorLabel.font = UIFont(name: "Arial", size: 15)
        errorLabel.textColor = UIColor.whiteColor()
        errorLabel.textAlignment = NSTextAlignment.Center
        errorLabel.alpha = 0
        self.view.addSubview(errorLabel)
        
    }
    
    func login() {
        
        let email = self.emailBox.text?.lowercaseString
        let password = self.passwordBox.text?.lowercaseString
        let result = isLoginReady(email, password: password)
        
        if result.ready {
            
            cleanError()
            loadingView.showCover()
            
            self.authManager.login(result.email!, password: result.password!) { (result:Bool) in
                
                self.loadingView.hide()
                
                if (result) {
                    self.dismiss()
                    NSNotificationCenter.defaultCenter().postNotificationName("USER_AUTHENTICATED", object: nil)
                } else {
                    self.showError("Login Failed")
                }
                
            }
            
        } else {
            
            self.loadingView.hide()
            showError("Please enter email and password")
        }
        
    }
    
    func isLoginReady(email:String?, password:String?) -> (ready: Bool, email: String?, password: String?) {
        
        if email == nil {
            return (false, nil, nil)
        }
        
        if password == nil {
            return (false, nil, nil)
        }
        
        let trimmedPhoneNumber = String.trimString(email!)
        if trimmedPhoneNumber.characters.count == 0 {
            return (false, nil, nil)
        }
        if  String.trimString(password!).characters.count == 0 {
            return (false, nil, nil)
        }
        
        return (true, trimmedPhoneNumber, password)
    }
    
    func showError(error:String) {
        
        self.errorLabel.alpha = 0.0
        self.errorLabel.text = error
        
        UIView.animateWithDuration(0.2, delay: 0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
            self.errorLabel.alpha = 1
            
            }, completion: {(Bool) in
        })
        
    }

    func dismiss() {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func cleanError() {
        
        self.errorLabel.text = ""
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
