//
//  Photo.swift
//  Flakes
//
//  Created by juan kou on 7/26/16.
//  Copyright © 2016 Flakes.com. All rights reserved.
//

import Foundation
import NYTPhotoViewer

class Photo: NSObject, NYTPhoto {
    var image:UIImage?
    var imageData: NSData?
    var placeholderImage: UIImage?
    var attributedCaptionTitle: NSAttributedString?
    var attributedCaptionSummary: NSAttributedString?
    var attributedCaptionCredit: NSAttributedString?
    
    init(img:UIImage) {
        self.image = img
    }
}
