//
//  ActiveFlakeTableViewCell.swift
//  Flakes
//
//  Created by Kelvin Deng on 7/24/16.
//  Copyright © 2016 Flakes.com. All rights reserved.
//

import UIKit

class InActiveFlakeTableViewCell: UITableViewCell {
    
    var userLocation:UILabel = UILabel()
    var userImg:UIImageView = UIImageView()
    var map:UIImageView = UIImageView()
    var bgView:UIView = UIView()
    let padding:CGFloat = 10
    let bgWidth:CGFloat = FlakesConstant.screenWidth - 10 * 2
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setBgView()
        //setUserLocationUI()
        setMapUI()
        self.backgroundColor = UIColor.TableViewBgColor
        self.selectionStyle = .None
    }
    
    func setBgView() {
        bgView.frame = CGRectMake(padding, padding, bgWidth, 210)
        bgView.backgroundColor = UIColor.whiteColor()
        self.contentView.addSubview(bgView)
        bgView.layer.borderWidth = 2
        bgView.layer.borderColor = UIColor.LightBlue.CGColor
    }
    
    func setUserLocationUI() {
        
        userLocation.frame = CGRectMake(padding, padding, bgWidth - padding * 2, 40)
        userLocation.textAlignment = .Left
        self.bgView.addSubview(userLocation)
    }
    
    func setMapUI() {
        map.frame = CGRectMake(0, 0, bgWidth, 210)
        //map.frame = CGRectMake(0, padding + userLocation.frame.height, bgWidth, 160)
        self.bgView.addSubview(map)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
}
