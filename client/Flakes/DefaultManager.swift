import Foundation
import UIKit
import Alamofire
import SwiftyJSON

private let _singletonInstance = DefaultManager()

public class DefaultManager: NSObject {
    
    private let TOKEN = "TOKEN"
    private let REQUEST_PUSH_NOTIFICATION = "REQUEST_PUSH_NOTIFICATION"
    private let PUSH_NOTIFICATION_TOKEN = "PUSH_NOTIFICATION_TOKEN"
    private let USER_NAME = "USER_NAME"
    private let USER_EMAIL = "USER_EMAIL"
    
    public class var sharedInstance:DefaultManager {
        return _singletonInstance
    }
    
    func pushNotificationRequested() -> Bool {
        return NSUserDefaults.standardUserDefaults().valueForKey(REQUEST_PUSH_NOTIFICATION) != nil
    }
    
    func markPushNotificationRequest() {
        NSUserDefaults.standardUserDefaults().setValue("requested", forKey: REQUEST_PUSH_NOTIFICATION)
        save()
    }
    
    func getPushNotificationToken() -> String? {
        
        if NSUserDefaults.standardUserDefaults().valueForKey(PUSH_NOTIFICATION_TOKEN) as? String == nil {
            return "Hello"
        } else {
            return NSUserDefaults.standardUserDefaults().valueForKey(PUSH_NOTIFICATION_TOKEN) as? String
        }
    
    }
    
    func savePushNotificationToken(token token:String) {
        NSUserDefaults.standardUserDefaults().setValue(token, forKey: PUSH_NOTIFICATION_TOKEN)
        save()
    }
    
    public func setToken(token:String) {
        NSUserDefaults.standardUserDefaults().setValue(token, forKey: TOKEN)
        save()
    }

    public func setUserName(userName:String) {
        NSUserDefaults.standardUserDefaults().setValue(userName, forKey: USER_NAME)
    }
    
    public func setUserEmail(userEmail:String) {
        NSUserDefaults.standardUserDefaults().setValue(userEmail, forKey: USER_EMAIL)
    }

    func isLoggedIn() -> Bool {
        return getToken() != nil
    }
    
    public func getToken() -> String? {
        return NSUserDefaults.standardUserDefaults().valueForKey(TOKEN) as? String
    }
    
    public func getUserName() -> String? {
        return NSUserDefaults.standardUserDefaults().valueForKey(USER_NAME) as? String
    }
    
    public func getUserEmail() -> String? {
        return NSUserDefaults.standardUserDefaults().valueForKey(USER_EMAIL) as? String
    }

    public func clearUserData() {
        NSUserDefaults.standardUserDefaults().removeObjectForKey(TOKEN)
        //NSUserDefaults.standardUserDefaults().removeObjectForKey(USER_EMAIL)
    }
    
    public func getServerConstant(str:String) -> String? {
        return NSUserDefaults.standardUserDefaults().valueForKey(str) as? String
    }
    
    private func save() {
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
}