//
//  LoginTableViewController.swift
//  Flakes
//
//  Created by juan kou on 7/25/16.
//  Copyright © 2016 Flakes.com. All rights reserved.
//


class LoginTableViewController: UITableViewController, UITextFieldDelegate {
    
    var emailCell: InputTableViewCell?
    var passwordCell: InputTableViewCell?
    lazy var authManager:AuthenticationManager = AuthenticationManager()
    let loadingView:LoadingView = LoadingView()
    
    lazy var errorLabel: UILabel = {
        
        let labelLeftPadding:CGFloat = 20.0
        let labelY:CGFloat = 80.0
        let labelHeight:CGFloat = 100.0
        var label:UILabel = UILabel(frame: CGRectMake(labelLeftPadding, labelY, FlakesConstant.screenWidth - 2 * labelLeftPadding, labelHeight))
        label.textAlignment = .Center
        label.textColor = UIColor.LightBlue
        label.lineBreakMode = .ByWordWrapping
        label.numberOfLines = 0
        return label
        
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "登陆"
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.tableView.backgroundColor = UIColor.whiteColor()
        self.tableView.registerClass(InputTableViewCell.classForCoder(), forCellReuseIdentifier: "InputCell")
        //self.tableView.backgroundColor = UIColor.VimPlaceholderColor
        self.tableView.tableFooterView = UIView(frame: CGRectZero)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: .Plain, target: self, action: #selector(login))
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .Plain, target: self, action: #selector(dismiss))

        self.view.addSubview(self.errorLabel)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(animated: Bool) {
        
        super.viewDidAppear(animated)
        if let ec = self.emailCell {
            ec.inputField.becomeFirstResponder()
        }
        
    }
    
    
    override func viewWillDisappear(animated: Bool) {
        
        super.viewWillDisappear(animated)
        if (self.isMovingFromParentViewController()) {
            
            self.navigationController?.setNavigationBarHidden(true, animated: true)
            self.emailCell?.inputField.resignFirstResponder()
            self.passwordCell?.inputField.resignFirstResponder()
            
        }
        
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell:InputTableViewCell = tableView.dequeueReusableCellWithIdentifier("InputCell", forIndexPath: indexPath) as! InputTableViewCell
        if indexPath.row == 0 {
            
            cell.configureForEmailInput()
            self.emailCell = cell
            self.emailCell!.inputField.tag = 0
            self.emailCell!.inputField.delegate = self
            
        } else {
            
            cell.configureForPasswordInput()
            self.passwordCell = cell
            self.passwordCell!.inputField.tag = 1
            self.passwordCell!.inputField.delegate = self
            
        }
        
        return cell;
        
    }
    
    // MARK: - Textfield delegate
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        if textField.tag == 0 {
            
            self.emailCell?.inputField.resignFirstResponder()
            self.passwordCell?.inputField.becomeFirstResponder()
            
        } else {
            
            login()
            
        }
        
        return true
    }
    
    func login() {
        
        let email = self.emailCell!.inputField.text
        let password = self.passwordCell!.inputField.text
        let result = isLoginReady(email, password: password)
        
        if result.ready {
            
            cleanError()
            loadingView.showCover()
            
            self.authManager.login(result.email!, password: result.password!) { (result:Bool) in
                
                self.loadingView.hide()
                
                if (result) {
                    self.dismiss()
                    NSNotificationCenter.defaultCenter().postNotificationName("USER_AUTHENTICATED", object: nil)
                } else {
                    self.showError("Login Failed")
                }
                
            }
            
        } else {
            
            self.loadingView.hide()
            showError("Please enter email and password")
        }
    
    }
    
    func isLoginReady(email:String?, password:String?) -> (ready: Bool, email: String?, password: String?) {
        
        if email == nil {
            return (false, nil, nil)
        }
        
        if password == nil {
            return (false, nil, nil)
        }
        
        let trimmedPhoneNumber = String.trimString(email!)
        if trimmedPhoneNumber.characters.count == 0 {
            return (false, nil, nil)
        }
        if  String.trimString(password!).characters.count == 0 {
            return (false, nil, nil)
        }
        
        return (true, trimmedPhoneNumber, password)
    }
    
    func showError(error:String) {
        
        self.errorLabel.alpha = 0.0
        self.errorLabel.text = error
        
        UIView.animateWithDuration(0.2, delay: 0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
            self.errorLabel.alpha = 1
            
            }, completion: {(Bool) in
        })
        
    }
    
    func dismiss() {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func cleanError() {
        
        self.errorLabel.text = ""
        
    }
    
    func didFailLogIn() {
        
        //showError(VimConstants.InvalidLoginInputs)
        
    }
    
    func didFailConnectToServer() {
        
        //showError(VimConstants.CannotConnectToServer)
        
    }
    
    func didLogIn() {
        self.emailCell!.inputField.resignFirstResponder()
        self.passwordCell!.inputField.resignFirstResponder()
        self.navigationController?.presentingViewController?.dismissViewControllerAnimated(true, completion: nil)
    }
    
    deinit {
        
        NSNotificationCenter.defaultCenter().removeObserver(self)
        
    }
    
}
