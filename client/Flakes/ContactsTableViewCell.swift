//
//  ContactsTableViewCell.swift
//  Flakes
//
//  Created by DuRuitao on 7/25/16.
//  Copyright © 2016 Flakes.com. All rights reserved.
//

import UIKit

class ContactsTableViewCell: UITableViewCell {
    
    var userName:UILabel = UILabel()
    var userGuid:UILabel = UILabel()
    let padding:CGFloat = 10
    let bgWidth:CGFloat = FlakesConstant.screenWidth - 10 * 2
    let leftWidth:CGFloat = (FlakesConstant.screenWidth - 10 * 2) / 3
    
    let myImgView:UIImageView = UIImageView()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.contentView.backgroundColor = UIColor.LightGray
        
        setUserName()
        setUserGuid()
    }
    
    func setUserName(){
        userName.frame = CGRectMake(padding+50, padding/2, bgWidth*2/3, 30)
        userName.textAlignment = .Center
        userName.textColor = UIColor.LightBlue
        userName.font = UIFont(name:"HelveticaNeue-Bold", size: 16.0)
        
        self.contentView.addSubview(userName)
        
        myImgView.frame = CGRectMake(padding, padding/2, 50, 50)
        self.contentView.addSubview(myImgView)
    }
    
    func setUserGuid(){
        userGuid.frame = CGRectMake(padding, padding*2+10, bgWidth, 20)
        userGuid.textAlignment = .Center;
        userGuid.textColor = UIColor.blackColor()
        self.contentView.addSubview(userGuid)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
