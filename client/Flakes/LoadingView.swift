//
//  LoadingView.swift
//  Flakes
//
//  Created by Kelvin Deng on 7/23/16.
//  Copyright © 2016 Flakes.com. All rights reserved.
//
import Foundation
import NVActivityIndicatorView

class LoadingView: NSObject {
    
    var bgView:UIView = UIView()
    
    override init() {
        
    }
    
    func showMiddle() {
        
        self.bgView.alpha = 1
        bgView.frame = CGRectMake((FlakesConstant.screenWidth - 120) / 2, (FlakesConstant.screenWidth - 80 ) / 2, 120, 120)
        
        let visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .Light))
        visualEffectView.frame = bgView.bounds
        //bgView.addSubview(visualEffectView)
        bgView.backgroundColor = UIColor.whiteColor()
        bgView.layer.cornerRadius = 10
        
        let height:CGFloat = 80
        let width:CGFloat = 80
        let left:CGFloat = 20
        let top:CGFloat = 20
        
        let loadingView = NVActivityIndicatorView(frame: CGRectMake(left, top, width, height), type: NVActivityIndicatorType.BallScaleMultiple, color: UIColor.LightBlue, padding: 10)
        
        bgView.addSubview(loadingView)
        
        UIApplication.sharedApplication().keyWindow!.addSubview(bgView)
        loadingView.startAnimation()
    }
    
    func showCover() {
        self.bgView.alpha = 1
        bgView.frame = CGRectMake(0, 0, FlakesConstant.screenWidth , FlakesConstant.screenHeight )
        
        let visualEffectView = UIVisualEffectView(effect: UIBlurEffect(style: .Light))
        visualEffectView.frame = bgView.bounds
        bgView.addSubview(visualEffectView)
        
        let height:CGFloat = 80
        let width:CGFloat = 80
        let left:CGFloat = (FlakesConstant.screenWidth - height ) / 2
        let top:CGFloat = (FlakesConstant.screenWidth - height ) / 2
        
        let loadingView = NVActivityIndicatorView(frame: CGRectMake(left, top, width, height), type: NVActivityIndicatorType.BallScaleMultiple, color: UIColor.LightBlue, padding: 10)
        
        bgView.addSubview(loadingView)

        UIApplication.sharedApplication().keyWindow!.addSubview(bgView)
        loadingView.startAnimation()
    }
    
    func hide() {
        
        UIView.animateWithDuration(1, animations: {
                self.bgView.alpha = 0
            }, completion: {
                (value: Bool) in
                self.bgView.removeFromSuperview()
        })
        
    }
}
