//
//  ActiveFlakeTableViewCell.swift
//  Flakes
//
//  Created by Kelvin Deng on 7/24/16.
//  Copyright © 2016 Flakes.com. All rights reserved.
//

import UIKit

class ActiveFlakeTableViewCell: UITableViewCell {
    
    var userName:UILabel = UILabel()
    var userImg:UIImageView = UIImageView()
    var map:UIImageView = UIImageView()
    var bgView:UIView = UIView()
    let padding:CGFloat = 10
    let bgWidth:CGFloat = FlakesConstant.screenWidth - 10 * 2
    let leftWidth:CGFloat = (FlakesConstant.screenWidth - 10 * 2) / 3

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setBgView()
        setUserNameUI()
        setMapUI()
        
        self.backgroundColor = UIColor.TableViewBgColor
        self.selectionStyle = .None
    }
    
    func setBgView() {
        bgView.frame = CGRectMake(padding, padding, bgWidth, 160)
        bgView.backgroundColor = UIColor.whiteColor()
        self.contentView.addSubview(bgView)
        
    }
    
    func setUserNameUI() {
    
        let padding:CGFloat = 20
        let imgWidth:CGFloat = leftWidth - padding * 2
        userImg.frame = CGRectMake(padding, padding, imgWidth, imgWidth)
        userImg.layer.cornerRadius = (leftWidth - padding * 2) / 2
        
        userImg.image = UIImage(named: "private")
        bgView.addSubview(userImg)
        
        let s_padding:CGFloat = 10
        let top:CGFloat = padding + imgWidth + s_padding
        userName.frame = CGRectMake(s_padding, top, leftWidth - s_padding * 2, 30)
        userName.textAlignment = .Center
        self.bgView.addSubview(userName)
    }
    
    func setMapUI() {
        map.frame = CGRectMake(leftWidth, 0, bgWidth - leftWidth, 160)
        self.bgView.addSubview(map)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }


}
