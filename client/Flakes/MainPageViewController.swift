//
//  MainPageViewController.swift
//  Flakes
//
//  Created by DuRuitao on 7/25/16.
//  Copyright © 2016 Flakes.com. All rights reserved.
//

import UIKit

class MainPageViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if (DefaultManager.sharedInstance.isLoggedIn()) {
            hide()
        }
        setUI()
//        let appDelegate = UIApplication.sharedApplication().delegate! as! AppDelegate
//        var initialViewController = self.storyboard!.instantiateViewControllerWithIdentifier("FlakeTabbarVC") as! UIViewController
//        appDelegate.window?.rootViewController = initialViewController
//        appDelegate.window?.makeKeyAndVisible()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(hide), name: "USER_AUTHENTICATED", object: nil)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUI() {
        
        self.view.backgroundColor = UIColor.LightBlue
        
        
        /*
         
         var imageView : UIImageView
         
         imageView  = UIImageView(frame:CGRectMake(FlakesConstant.screenWidth/10, FlakesConstant.screenHeight/3-110, 30, 30))
         
         imageView.image = UIImage(named: "icon")
         
         authenticatePage.addSubview(imageView)*/
        
        let textField = UILabel(frame: CGRectMake(FlakesConstant.screenWidth/10,FlakesConstant.screenHeight/3-70,FlakesConstant.screenWidth*8/9, 60))
        
        textField.text = "Welcome to SnowFLAKE."
        
        textField.font = UIFont(name: "Arial", size: 30)
        
        textField.textColor = UIColor.whiteColor()
        
        self.view.addSubview(textField)
        
        
        
        let loginButton = UIButton(frame: CGRectMake(FlakesConstant.screenWidth/10, FlakesConstant.screenHeight/3+10, FlakesConstant.screenWidth*8/10, 40))
        
        loginButton.backgroundColor = UIColor.whiteColor()
        
        loginButton.layer.borderColor = UIColor.LightBlue.CGColor
        
        loginButton.layer.borderWidth = 1.5
        
        loginButton.layer.cornerRadius = 10
        
        loginButton.setTitle("Log In", forState: .Normal)
        
        loginButton.setTitleColor(UIColor.LightBlue, forState: .Normal)
        
        self.view.addSubview(loginButton)
        
        
        loginButton.addTarget(self, action: #selector(showLoginVC), forControlEvents: .TouchUpInside)
        
        
        let signupButton = UIButton(frame: CGRectMake(FlakesConstant.screenWidth/10, FlakesConstant.screenHeight/3+60, FlakesConstant.screenWidth*8/10, 40))
        
        signupButton.layer.cornerRadius = 10
        
        signupButton.layer.borderWidth = 1.5
        
        signupButton.backgroundColor = UIColor.LightBlue
        
        signupButton.layer.borderColor = UIColor.whiteColor().CGColor
        
        signupButton.setTitle("Create Account", forState: .Normal)
        
        signupButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        
        self.view.addSubview(signupButton)
        
        
        signupButton.addTarget(self, action: #selector(showSignupVC), forControlEvents: .TouchUpInside)
    }
    
    func hide() {
        let appDelegate = UIApplication.sharedApplication().delegate! as! AppDelegate
        let initialViewController = self.storyboard!.instantiateViewControllerWithIdentifier("FlakeTabbarVC")
        appDelegate.window?.rootViewController = initialViewController
        appDelegate.window?.makeKeyAndVisible()
    }
    
    func showLoginVC() {
        
//        let vc = LoginTableViewController()
//        let navController = UINavigationController(rootViewController: vc)
//        UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(navController, animated: true, completion: nil)
        
        let vc = SignInViewController()
        UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(vc, animated: true, completion: nil)
        
    }
    
    func showSignupVC() {
//        let vc = SignupTableViewController()
//        let navController = UINavigationController(rootViewController: vc)
//        UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(navController, animated: true, completion: nil)
        
        let vc = SignupViewController()
        UIApplication.sharedApplication().keyWindow?.rootViewController?.presentViewController(vc, animated: true, completion: nil)

    
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
