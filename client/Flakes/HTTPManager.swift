//
//  HTTPManager.swift
//  Flakes
//
//  Created by juan kou on 7/23/16.
//  Copyright © 2016 Flakes.com. All rights reserved.
//

import Foundation
import Alamofire

private let _sharedInstance = HTTPManager()

class HTTPManager: NSObject {
    var authToken:String?
    var manager:Alamofire.Manager?
    
    override init() {
        super.init()
        //check if auth token is already set
        
        let authToken = "Janeisthebest"
        self.createManagerWithAuthToken(authToken)
        
        //SKIP
        
        /*
        let authToken = DefaultManager.sharedInstance.getAuthToken()
        if let authToken = authToken {
            self.createManagerWithAuthToken(authToken)
        } else {
            fatalError("Cannot call HTTPManager before authentication")
        }
 
         */
    }
    
    func createManagerWithAuthToken(token:String) {
        var defaultHeaders = Alamofire.Manager.sharedInstance.session.configuration.HTTPAdditionalHeaders ?? [:]
        defaultHeaders["X-AUTH-TOKEN"] = token
        let configuration = NSURLSessionConfiguration.defaultSessionConfiguration()
        configuration.HTTPAdditionalHeaders = defaultHeaders
        manager = Alamofire.Manager(configuration: configuration)
    }
    
    class var sharedInstance:HTTPManager {
        return _sharedInstance
    }
}

