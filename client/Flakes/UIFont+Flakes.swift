//
//  UIFont+Flakes.swift
//  Flakes
//
//  Created by juan kou on 7/25/16.
//  Copyright © 2016 Flakes.com. All rights reserved.
//

import UIKit

extension UIFont {
    
    static public func FlakesFont(size size:CGFloat) -> UIFont {
        return UIFont (name: "HelveticaNeue-Light", size: size)!
    }
    
}
