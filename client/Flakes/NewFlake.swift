//
//  NewFlake.swift
//  Flakes
//
//  Created by Kelvin Deng on 7/23/16.
//  Copyright © 2016 Flakes.com. All rights reserved.
//

import UIKit

class NewFlake: NSObject {
    var content:String
    var question:String?
    var answer:String?
    var latitude:Double
    var longitude:Double
    var isPublic:Bool
    var receiver:String?
    
    init(isPublic:Bool, latitude:Double, longitude:Double, content:String) {
        self.isPublic = isPublic
        self.latitude = latitude
        self.longitude = longitude
        self.content = content
        super.init()
    }
}
