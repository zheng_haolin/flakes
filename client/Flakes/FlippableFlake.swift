//
//  FlippableFlake.swift
//  Flakes
//
//  Created by DuRuitao on 7/24/16.
//  Copyright © 2016 Flakes.com. All rights reserved.
//

import Foundation

import UIKit

class FlippableFlake {
    
    var flakeGuid:String
    var userGuid:String
    var userName:String
    var latitude:Double
    var longitude:Double
    var createTimeStamp:String
    var questionGuid:String?
    var questionCorrectAnswer:String?
    var openCount:Int
    var content:String
    var receiver:String?
    
    init(flakeGuid:String, userGuid:String, userName:String, latitude:Double,longitude:Double, createTimeStamp:String, questionGuid:String?, questionCorrectAnswer:String?, openCount:Int, content:String) {
        
        self.flakeGuid = content
        self.userGuid = userGuid
        self.userName = userName
        self.latitude = latitude
        self.longitude = longitude
        self.createTimeStamp = createTimeStamp
        
        self.questionGuid = questionGuid
        self.questionCorrectAnswer = questionCorrectAnswer
        self.openCount = openCount
        self.content = content
    }
    
    init(flakeGuid:String, userGuid:String, userName:String, latitude:Double,longitude:Double, createTimeStamp:String, questionGuid:String?, questionCorrectAnswer:String?, openCount:Int, content:String, receiver:String) {
        
        self.flakeGuid = content
        self.userGuid = userGuid
        self.userName = userName
        self.latitude = latitude
        self.longitude = longitude
        self.createTimeStamp = createTimeStamp
        
        self.questionGuid = questionGuid
        self.questionCorrectAnswer = questionCorrectAnswer
        self.openCount = openCount
        self.content = content
        self.receiver = receiver
    }
    
    init(flakeGuid:String, userGuid:String, userName:String, latitude:Double,longitude:Double, createTimeStamp:String, openCount:Int, content:String) {
        
        self.flakeGuid = content
        self.userGuid = userGuid
        self.userName = userName
        self.latitude = latitude
        self.longitude = longitude
        self.createTimeStamp = createTimeStamp
        
        self.openCount = openCount
        self.content = content
    }
}
