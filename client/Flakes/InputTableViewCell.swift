//
//  InputTableViewCell.swift
//  vim-toys
//
//  Created by juan kou on 1/22/16.
//  Copyright © 2016 vim.com. All rights reserved.
//

import UIKit

class InputTableViewCell: UITableViewCell {
    
    var inputField:UITextField = UITextField()

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addTextField()
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureForEmailInput() {
        setKeyboardType(UIKeyboardType.EmailAddress)
        inputField.placeholder = "Email:"
        inputField.autocapitalizationType = UITextAutocapitalizationType.None
        inputField.autocorrectionType = UITextAutocorrectionType.No
        inputField.spellCheckingType = UITextSpellCheckingType.No
        inputField.returnKeyType = UIReturnKeyType.Next
        inputField.clearButtonMode = UITextFieldViewMode.WhileEditing
    }
    
    func configureForPasswordInput() {
        setKeyboardType(UIKeyboardType.Default)
        inputField.placeholder = "Password:"
        inputField.returnKeyType = UIReturnKeyType.Done
        inputField.clearButtonMode = UITextFieldViewMode.WhileEditing
        inputField.secureTextEntry = true
    }
    
    func configureForPasswordConfirmationInput() {
        setKeyboardType(UIKeyboardType.Default)
        inputField.placeholder = "Confirm Password:"
        inputField.returnKeyType = UIReturnKeyType.Next
        inputField.clearButtonMode = UITextFieldViewMode.WhileEditing
        inputField.secureTextEntry = true
    }
    
    func addTextField() {
        
        let screenWidth:CGFloat = UIApplication.sharedApplication().keyWindow!.frame.size.width
        inputField.frame = CGRectMake(20, 0, screenWidth-40, 50)
        inputField.selected = false
        contentView.addSubview(inputField)
    }
    
    func setKeyboardType(keyboardType:UIKeyboardType) {
        inputField.keyboardType = keyboardType
        
        //Font is subject to change
        
    }

}
